!-----------------------------------------------------------------------
!
!    Copyright 2008-2021
!    Kerry Key
!    Lamont-Doherty Earth Observatory, Columbia University
!    kkey@ldeo.columbia.edu
!
!    This file is part of MARE2DEM.
!
!    MARE2DEM is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    MARE2DEM is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MARE2DEM.  If not, see <http://www.gnu.org/licenses/>.
!
!-----------------------------------------------------------------------


!==================================================================================================================================!
!======================================================================================================================== computeFwd
!==================================================================================================================================!
subroutine computeFwd( bDoPartials, currentMod )
!
! Routine to compute the 2D forward response and model Jacobian matrix
! This is called from Occam.
!
    use Occam, only: nd, wj, RealPrec         ! Passes out dm and wj here
    use kx_io           ! for rhoParams
    use mare2dem_global
    use mare2dem_mpi

    implicit none

!
! Arguments
!
    logical, intent(in)        :: bDoPartials         ! logical flag for computing Jacobians
    real(RealPrec), intent(in) :: currentMod(nFree)   ! input model parameters

!
! Local variables:
!
    integer :: i
    external ::  insertFreeParams, compute_DataWeights,estimate_data_corrections
!
! Copy the model parameters (log10(resistivity)) in currentMod into the resistivity array used by the fwd code:
!
    call insertFreeParams(currentMod)

!
! Compute responses and sensitivities for 2D CSEM and MT data using the MARE2DEM parallel finite element kernel:
!
    linversion = bDoPartials
    if (lPrintDebug) write(*,*) 'calling mpi_mare2dem...'

    call mpi_mare2dem

!
! Estimate data corrects if requested: MT static shifts, CSEM timing errors or complex coefficients
!
    if (lPrintDebug) write(*,*) 'estimating data corrections...'

    call estimate_data_corrections

    !
    ! If inversion Jacobian call, there may be a few extra steps to perform:
    !
    if (bDoPartials) then

        ! Update data weights used for joint inversion:
        call compute_DataWeights()

        ! Check for nan's and infinity and replace them with 0's so the inversion can plow through:
        do i = 1,nd
            where ( wj(i,:) /=  wj(i,:)   )  wj(i,:) = 0d0   ! replace nan's with 0 ! kwk debug: should put warning about this!
            where ( wj(i,:) >=  huge(1d0) )  wj(i,:) = 0d0    ! replace +infinity with 0
            where ( wj(i,:) <= -huge(1d0) )  wj(i,:) = 0d0    ! replace -infinity with 0
        enddo

    endif



    if (lPrintDebug) write(*,*) 'leaving computeFwd in EM2D.f90...'

end subroutine computeFwd

!==================================================================================================================================!
!============================================================================================================ displayJointInvMisfits
!==================================================================================================================================!
subroutine displayJointInvMisfits()
!
! Prints joint inversion misfits (if joint inversion). Also shows estimated MT static shifts.
!
    use Occam, only: nd,sd,d,dm,currentIteration, printOccamLog, RealPrec, modelRMS, pm_assoc
    use mare2dem_global
    use mare2dem_input_data_params

    implicit none

    character(256)  :: cStr, cFilename
    logical         :: lFileExists
    integer         :: i,n,ierr, IOunit
    real(RealPrec), dimension(:), allocatable :: rms_grp


    if (n_grp > 1) then

        !
        ! Compute RMS misfit for each group:
        !
        allocate(rms_grp(n_grp))
        rms_grp = 0.
        do i = 1,nd
            rms_grp(d_grp(i)) = rms_grp(d_grp(i)) + ( (d(i) - dm(i)) /sd(i))**2
        enddo
        do i = 1,n_grp
            n = count(d_grp == i)
            rms_grp(i) = sqrt(rms_grp(i)/n)
        enddo

        !
        ! Print to screen:
        !

        write(cStr,'(a)') 'Joint EM Data Type Inversion Misfits:'
        call printOccamLog(cStr)
        do i = 1,n_grp
            write(cStr,'(a23,1x,a,1x,g16.4)') trim(grp_names(i)), 'Misfit:',rms_grp(i)
            call printOccamLog(cStr)
        enddo

        !--------------------------------------------
        ! Save joint inversion misfits to a file too:
        !
        !write (cNum,*) nCurrentIter
        !cFileName = trim(outputFileRoot)//'.'//trim(adjustl(cNum))//'.group_rms_log'
        cFileName = trim(outputFileRoot)//'.group_rms.log'

        ! See if it exists:
        inquire(file=cFileName,exist=lFileExists)

        ! If the file exists then append the new log to it:
        if ( (currentIteration > 0).and.(lFileExists) ) then
            open (newunit=IOunit, file=cFileName, position='append',iostat=ierr)

            if (ierr .ne. 0) then
                write(cStr,'(a,a)') ' Error opening group rms log file:', trim(cFileName)
                call printOccamLog(cStr)
                stop
            endif

        else  ! Open a new file
            open (newunit=IOunit, file=cFileName, iostat=ierr)

            if (ierr .ne. 0) then
                write(cStr,'(a,a)') ' Error opening group rms log file:', trim(cFileName)
                call printOccamLog(cStr)
                stop
            endif

            ! write header line:
            write(cStr,'(a12,1x,a24)') 'Iteration,', 'Total RMS'
            do i = 1,n_grp
                write(cStr,'(a,a,a24)') trim(cStr),',',trim(grp_names(i))
            enddo
            write(IOunit,'(a)') trim(cStr)

        endif

        write(IOunit,'(i12,1x,10(f24.3,1x))') currentIteration, modelRMS,(rms_grp(i),i=1,n_grp)

        close(IOunit)

        deallocate(rms_grp)

    endif

    !
    ! Display any MT static shift solutions:
    !
    if ( nRxMT > 0 ) then

        if (any(iEstimateMTStatic > 0)) then

            write(cStr,'(a)') ' MT static shift estimates: '
            call printOccamLog(cStr)
            write(cStr,'(4(a12,2x),a34)') ' Site #' ,'Name', 'TE Factor' , 'TM Factor', ' where linear ApRes = ApRes*Factor'
            call printOccamLog(cStr)
            do i = 1,nRxMT
                if (iEstimateMTStatic(i) > 0) then
                    write(cStr,'(i12,2x,a12,2x,f12.3,2x,f12.3)') i, trim(cRxNamesMT(i)), 10**pm_assoc(2*i-1), 10**pm_assoc(2*i) ! kwk debug: need to update for ZDET data type
                    call printOccamLog(cStr)
                endif
            enddo

        endif

    endif

end subroutine displayJointInvMisfits

!==================================================================================================================================!
!================================================================================================================== insertFreeParams
!==================================================================================================================================!
subroutine insertFreeParams(currentMod)

    use mare2dem_global
    use Occam           ! Passes out dm and wj here
    use kx_io           ! for rhoParams

    implicit none

    real(RealPrec), intent(in) :: currentMod(nFree)   ! input model parameters

    integer :: i,j, ict

    ict = 0

    select case (trim(cAnisotropy ))

      case ('isotropic','tix','tiy','tiz','tiz_ratio','triaxial','isotropic_complex')
        do i=1,nRegions
            do j = 1,nRhoPerRegion
                if (iFreeParam((i-1)*nRhoPerRegion + j) > 0 ) then
                    ict = ict+1
                    rhoParams((i-1)*nRhoPerRegion + j) = 10d0**currentMod(ict) ! convert log10 to linear
                endif
            enddo
        enddo

      case ('isotropic_ip') ! Cole-Cole Model

        do i=1,nRegions
            do j = 1,nRhoPerRegion
                if (iFreeParam((i-1)*nRhoPerRegion + j) > 0 ) then
                    ict = ict+1

                    ! log:
                    if (j==1) then
                        rhoParams((i-1)*nRhoPerRegion + j) = 10d0**currentMod(ict) ! Rho
                    else
                        ! log:
                        !rhoParams((i-1)*nRhoPerRegion + j) = (10d0**currentMod(ict)) ! Eta, Tau, C
                        ! linear:
                        rhoParams((i-1)*nRhoPerRegion + j) = currentMod(ict) ! linear Eta, Tau, C
                    endif

                endif
            enddo

        enddo

    end select

end subroutine insertFreeParams

!==================================================================================================================================!
!======================================================================================================= generate_data_misfit_groups
!==================================================================================================================================!
subroutine generate_data_misfit_groups
    !
    ! Sets d_grp and grp_names for joint inversion of multiple EM data types, if it is not already user-specified in dataGroupFileName
    ! That file allows for custom data groups for misfit balancing during inversion and could be used for
    ! separately grouping nodal CSEM and towed CSEM data, etc. Here the data is only coarsely grouped as CSEM, MT and DC
    ! groups.
    !
    use mare2dem_global, only : lPrintDebug,lprintData
    use Occam,           only : nd, dp, printOccamLog
    use mare2dem_global, only : n_grp, d_grp, grp_names


    integer         :: i, imt, icsem, idc

    if (lPrintDebug) write(*,*) 'entering generate_data_misfit_groups...'


    if ( .not.(allocated(d_grp)) ) then

        n_grp = 0

        ! Scan data for more than one type (MT, CSEM, DC):
        imt = 0; icsem = 0; idc = 0
        do i = 1,nd
            if ( dp(i,1) < 100)                          icsem = 1
            if ( (dp(i,1) > 100) .and. (dp(i,1) < 200) ) imt   = 1
            if ( (dp(i,1) > 200) .and. (dp(i,1) < 300) ) idc   = 1
        enddo

        ! sum to get number of EM data types:
        n_grp = imt + icsem + idc

        if (n_grp > 1) then

            ! set up group names:
            allocate(grp_names(n_grp))
            if (icsem == 1) grp_names(icsem)             = 'CSEM'
            if (imt   == 1) grp_names(icsem + imt)       = 'MT'
            if (idc   == 1) grp_names(icsem + imt + idc) = 'DC Resistivity'

            ! set up group indices:
            allocate(d_grp(nd))
            do i = 1,nd
                if ( dp(i,1) < 100)                          d_grp(i) =  icsem
                if ( (dp(i,1) > 100) .and. (dp(i,1) < 200) ) d_grp(i) =  icsem + imt
                if ( (dp(i,1) > 200) .and. (dp(i,1) < 300) ) d_grp(i) =  icsem + imt + idc
            enddo

            ! Print info:
            if (lprintData) then
                write(*,*) '======== Generating Data Type Groups for Joint EM Data Inversion ========='
                write(*,*) ' '

                write(*,'(a)') 'Auto-detected multiple EM data types: '

                do i = 1,n_grp
                    write(*,'(i4,a,i6,2x,a,a)') i,  ' # data: ',count(d_grp == i),' Group name: ',trim(grp_names(i))
                enddo
                write(*,*) ' '

            endif
        endif

    endif

end subroutine generate_data_misfit_groups

!==================================================================================================================================!
!=============================================================================================================== compute_DataWeights
!==================================================================================================================================!
subroutine compute_DataWeights
!
! Computes weights uses to help balance the inversion of joint EM data types when
! there are more of one data type than another or other data types.
!
! The data weights are are applied in Occam.f90 as a scalar multiple of the data uncertainty, so the weights here are
!  squared in the functional that is being minimized.
!

    use Occam
    use mare2dem_global
    use mare2dem_input_data_params
    use mare2dem_io

    implicit none

    integer         :: i,n
    real(RealPrec)  :: chi_target

    real(RealPrec), dimension(:), allocatable   :: rms_grp, weights

    if (lPrintDebug) write(*,*) 'compute_DataWeights...'

    !
    ! Modify data weights if joint CSEM/MT inversion:
    !

    d_wt = 1.0

    if (n_grp > 1) then

        !
        ! Compute RMS misfit for each group:
        !
        allocate(rms_grp(n_grp),weights(n_grp))
        rms_grp = 0
        do i = 1,nd
            rms_grp(d_grp(i)) = rms_grp(d_grp(i)) + ( (d(i) - dm(i)) /sd(i))**2
        enddo
        do i = 1,n_grp
            n = count(d_grp == i)
            rms_grp(i) = sqrt(rms_grp(i)/n)
        enddo

        !
        ! Compute balancing weights:
        !
        do i = 1,n_grp

            n = count(d_grp == i)
            chi_target = sqrt(dble(n))

            select case (joint_inversion_weight_type)

              case ('unity')
                weights(i) = 1.

              case ('data_count')  ! weights set to 1/sqrt(n) so normalized chi^2 for each data group should be ~1 for a model that fits all data types well
                weights(i) = 1./chi_target

              case ('misfit_balanced_data_count')  ! data_count scaled by current rms for this data group to encourage inversion to work on worse fit group(s)
                weights(i) = rms_grp(i)/chi_target

              case ('misfit') ! scale by current rms for this data group to encourage inversion to work on worse fit group(s)
                weights(i) = rms_grp(i)

              case default
                write(*,*) 'Error in subroutine compute_DataWeights not recognized'
                write(*,*) ' joint_inversion_weight_type not recognized: ',trim(joint_inversion_weight_type)
                stop
            end select

        enddo

        !
        ! Print to screen:
        !
        write(*,*) ''
        write(*,'(a)') 'Weights for joint inversion of multiple data types:'
        do i = 1,n_grp
            write(*,'(a,1x,g12.3,1x,a,2x,a)') 'Weight:', weights(i), 'Group:',trim(grp_names(i))
        enddo
        write(*,*) ''

        !
        ! Finally insert weights for each data point:
        !
        do i = 1,nd
            d_wt(i) = weights(d_grp(i))
        enddo

        ! Deallocate local arrays:
        deallocate(rms_grp,weights)

    endif

end subroutine compute_DataWeights

!==================================================================================================================================!
!======================================================================================================== estimate_data_corrections
!==================================================================================================================================!
subroutine estimate_data_corrections
!
! Routine that estimates data corrections during inversion, if requested in the data file:
! MT static shifts, CSEM timing errors and complex coefficients (per Rx or Tx)
!
    implicit none

    external ::  estimate_MT_staticShifts, estimate_CSEM_complex_corrections, estimate_CSEM_timing_corrections

! MT Static shifts:
    call estimate_MT_staticShifts

! CSEM complex coefficients:
    call estimate_CSEM_complex_corrections

! CSEM timing errors:
    call estimate_CSEM_timing_corrections


end subroutine estimate_data_corrections
!==================================================================================================================================!
!========================================================================================================= estimate_MT_staticShifts
!==================================================================================================================================!
subroutine estimate_MT_staticShifts
!
! A simplified static shift estimator based solely on the size of the mean residual.
!
! ****** USE WITH CAUTION! *****
!
    use Occam
    use mare2dem_input_data_params

    implicit none

    include 'em_parameters.inc'

    integer         :: iRx, i, nte, ntm, ndet

    real(RealPrec)  :: tesum, tmsum, detsum

!
! Solve for the static shift parameters at each site, if requested:
!
    do iRx = 1,nRxMT

        tesum     = 0
        tmsum     = 0
        detsum    = 0

        pm_assoc(2*iRx-1:2*iRx) = 0

        if (iEstimateMTStatic(iRx) /= 0) then

            nte = 0
            ntm = 0
            ndet = 0

            do i=1,nd

                if ( dp(i,4)  == iRx ) then

                    select case (  dp(i,1) )  ! what data type is this?   Note this assumes all data at site is of same linear or log10 scaling

                      case (indRhoZXY)    ! TE Apparent resistivity
                        tesum = tesum + log10(d(i)) - log10(dm(i))
                        nte   = nte + 1
                      case (indlog10RhoZXY)
                        tesum = tesum + d(i) - dm(i)
                        nte   = nte + 1
                      case (indRhoZYX)   ! TM Apparent resistivity
                        tmsum = tmsum + log10(d(i)) - log10(dm(i))
                        ntm   = ntm + 1
                      case (indlog10RhoZYX)
                        tmsum = tmsum + d(i) - dm(i)
                        ntm   = ntm + 1
                      case (indRhoZDET)   ! TM Apparent resistivity
                        detsum = detsum + log10(d(i)) - log10(dm(i))
                        ndet   = ndet + 1
                      case (indlog10RhoZDET)
                        detsum = detsum + d(i) - dm(i)
                        ndet   = ndet + 1
                    end select
                endif
            enddo
            if (nte > 0 ) then
                tesum = tesum / nte
            endif
            if (ntm > 0 ) then
                tmsum = tmsum / ntm
            endif
            if (ndet > 0 ) then
                detsum = detsum / ndet
            endif
            ! If only TE or TM statics requested, zero the other static estimate:
            if (iEstimateMTStatic(iRx) == 2) then ! TE only
                tmsum = 0
            elseif (iEstimateMTStatic(iRx) == 3) then ! TM only
                tesum = 0
            endif

            !write(*,*) 'tesum, tmsum: ',iRx, tesum, tmsum

            ! Store the static shifts:
            if (ndet == 0 ) then
                pm_assoc(2*iRx-1) = tesum
                pm_assoc(2*iRx  ) = tmsum
            else
                pm_assoc(2*iRx-1) = detsum ! only one mode but we save them here in case.
                pm_assoc(2*iRx  ) = detsum
            endif

            ! Finally do a second pass to apply the static shifts to the forward responses:
            do i=1,nd

                if ( dp(i,4)  == iRx ) then

                    select case (  dp(i,1) )  ! what data type is this?

                      case (indRhoZXY)    ! TE Apparent resistivity
                        dm(i) = dm(i)*10**tesum
                      case (indlog10RhoZXY)
                        dm(i) = dm(i) + tesum
                      case (indRhoZYX)   ! TM Apparent resistivity
                        dm(i) = dm(i)*10**tmsum
                      case (indlog10RhoZYX)
                        dm(i) = dm(i) + tmsum
                      case (indRhoZDET)   ! Determinant Apparent resistivity
                        dm(i) = dm(i)*10**detsum
                      case (indlog10RhoZDET)
                        dm(i) = dm(i) + detsum

                    end select
                endif
            enddo
        endif
    enddo

end subroutine estimate_MT_staticShifts

!==================================================================================================================================!
!================================================================================================= estimate_CSEM_complex_corrections
!==================================================================================================================================!
subroutine estimate_CSEM_complex_corrections
!
! kwk debug: !WARNING! This is a beta code for estimating Rx and/or Tx complex coefficients to approximate borehole casing
!            effects at a given frequency. This code assumes input data is a single data type.
!
!
!  Estimates complex correction coefficient at a given frequency all data for a given receiver or transmitter
!
!     Finds least squares solution for complex scalar C in complex error model: Z_D = Z_M C
!     where Z_D and Z_M are all the COMPLEX data for a given Rx (or Tx)
!     A weighted least squares solution computed using C = (W*Z_M)\(W*Z_D) where W is the inverse standard error.
!     C is estimated independently for each frequency of data.
!

    use occam
    use mare2dem_input_data_params
    use mare2dem_common, only: ismember

    implicit none

    include 'em_parameters.inc'

    external :: get_complex_vector, apply_complex_coeff, zgels

    integer         :: iRx, iTx, iFreq, nzd, i0,i,info

    complex(8), dimension(:), allocatable :: zd, zdm
    real(8), dimension(:), allocatable    :: wzd

    complex(8) :: work(128)

    integer :: idatatype, thistype ! 1-6 is Ex,Ey,Ez,Bx,By,Bz

    if ( (.not.any(iEstimateRxCorrection > 0)) .and.  (.not.any(iEstimateTxCorrection>0)) ) return
!
! Parse data types to make sure only a single component is being inverted since code is only set up for that
! at the moment:
!
    idatatype = 0
    do i = 1,nd

        thistype = 0
        select case (  dp(i,1) )  ! what data type is this?
          case (indAmpEx,indLog10AmpEx,indPhsEx,indRealEx,indImagEx)
            thistype = 1
          case (indAmpEy,indLog10AmpEy,indPhsEy,indRealEy,indImagEy)
            thistype = 2
          case (indAmpEz,indLog10AmpEz,indPhsEz,indRealEz,indImagEz)
            thistype = 3
          case (indAmpBx,indLog10AmpBx,indPhsBx,indRealBx,indImagBx)
            thistype = 4
          case (indAmpBy,indLog10AmpBy,indPhsBy,indRealBy,indImagBy)
            thistype = 5
          case (indAmpBz,indLog10AmpBz,indPhsBz,indRealBz,indImagBz)
            thistype = 6
        end select

        if ((thistype > 0) .and. (idatatype == 0)) idatatype = thistype

        if ( (thistype > 0) .and. (thistype /= idatatype) ) then
            write(*,*) ' '
            write(*,*) 'Error in subroutine estimate_CSEM_complex_corrections!'
            write(*,*) 'This routine only works when the data file has a single component type,'
            write(*,*) 'whereas the input data file has more than one field component type.'
            write(*,*) 'MARE2DEM will quit now. Please revise data file to have a single component,'
            write(*,*) 'or set the Rx and/or Tx correction flags to be zero.'
            stop

        endif

    enddo


!-------------------------------------------------
! Solve for any requested Rx complex coefficients:
!-------------------------------------------------
    if (allocated(iEstimateRxCorrection)) then

        allocate(wzd(nTxCSEM),zd(nTxCSEM),zdm(nTxCSEM))

        do iRx = 1,nRxCSEM

            if (iEstimateRxCorrection(iRx) == 2) then  ! gather all data and create complex Z_D for each frequency...

                do iFreq = 1,nFreqCSEM

                    ! Get complex data for this Rx and Freq:
                    nzd = nTxCSEM
                    call get_complex_vector(iRx,.true.,iFreq,zd,zdm,wzd,nzd)

                    !write(*,*) 'Rx correction: iRx,iFreq, nzd: ', iRx,iFreq, nzd

                    if (nzd > 0) then

                        ! write(*,*) 'iRx,iFreq, nzd: ', iRx,iFreq, nzd
!                     write(*,*) 'Linear system zd(real,imag), zdm(real,imag), weights:'
!                     do i = 1,nzd
!                         write(*,'(2(2(es22.15,1x),2x),5x,es22.15)') dble(zd(i)),dimag(zd(i)),dble(zdm(i)),dimag(zdm(i)), wzd(i)
!                     enddo

                        ! Solve for complex distortion:
                        zd  = zd/wzd  ! weight by uncertainty
                        zdm = zdm/wzd


                        ! ZGELS( TRANS, M, N, NRHS, A, LDA, B, LDB, WORK, LWORK, INFO )
                        call zgels( 'n', nzd, 1, 1, zdm, nzd, zd, nzd, work, size(work,1), info )

                        !  store the complex coefficient estimate:

                        i0 = nRxMT + (iRx-1)*nFreqCSEM + iFreq
                        pm_assoc(2*i0-1) = dble(zd(1))
                        pm_assoc(2*i0  ) = dimag(zd(1))

                        write(*,*) 'Complex coeff for iRx,iFreq, info: ',iRx,iFreq, info
                        write(*,*) '    C:' ,dble(zd(1)),dimag(zd(1))
                        write(*,*) '    RMS:' , sqrt( sum( abs(zd(2:nzd))**2) /nzd)

                        !
                        ! Now apply to dm:
                        !
                        call apply_complex_coeff(iRx,.true.,iFreq,zd(1))


                    endif

                enddo
            endif
        enddo

        deallocate(wzd,zd,zdm)
    endif

!-------------------------------------------------
! Solve for any requested Tx complex coefficients:
!-------------------------------------------------
    if (allocated(iEstimateTxCorrection)) then

        allocate(wzd(nRxCSEM),zd(nRxCSEM),zdm(nRxCSEM))

        do iTx = 1,nTxCSEM

            if (iEstimateTxCorrection(iTx) == 2) then  ! gather all data and create complex Z_D for each frequency...

                do iFreq = 1,nFreqCSEM

                    ! Get complex data for this Rx and Freq:
                    nzd = nRxCSEM
                    call get_complex_vector(iTx,.false.,iFreq,zd,zdm,wzd,nzd)

!                write(*,*) 'Tx correction: iTx,iFreq, nzd: ', iTx,iFreq, nzd

                    if (nzd > 0) then

!                     write(*,*) 'Linear system zd(real,imag), zdm(real,imag), weights:'
!                     do i = 1,nzd
!                         write(*,'(2(2(es22.15,1x),2x),5x,es22.15)') dble(zd(i)),dimag(zd(i)),dble(zdm(i)),dimag(zdm(i)), wzd(i)
!                     enddo

                        ! Solve for complex distortion:
                        zd  = zd/wzd  ! weight by uncertainty
                        zdm = zdm/wzd


                        ! ZGELS( TRANS, M, N, NRHS, A, LDA, B, LDB, WORK, LWORK, INFO )
                        call zgels( 'n', nzd, 1, 1, zdm, nzd, zd, nzd, work, size(work,1), info )

                        !  store the complex coefficient estimate:
                        i0 = nRxMT + nFreqCSEM*nRxCSEM + (iTx-1)*nFreqCSEM + iFreq
                        pm_assoc(2*i0-1) = dble(zd(1))
                        pm_assoc(2*i0  ) = dimag(zd(1))

                        write(*,*) 'Complex coeff for iTx,iFreq, info: ',iTx,iFreq, info
                        write(*,*) '    C:' ,dble(zd(1)),dimag(zd(1))
                        write(*,*) '    RMS:' , sqrt( sum( abs(zd(2:nzd))**2) /nzd)

!
                        ! Now apply to dm:
!
                        call apply_complex_coeff(iTx,.false.,iFreq,zd(1))


                    endif

                enddo
            endif
        enddo

        deallocate(wzd,zd,zdm)

    endif

end subroutine estimate_CSEM_complex_corrections

!==================================================================================================================================!
!================================================================================================================ get_complex_vector
!==================================================================================================================================!

subroutine get_complex_vector(ind,brx,ifreq,zd,zdm,wzd,nzd)
!
! Retrieves complex data vector at frequency index ifreq
! ind is Rx index if brx = .true. else Tx index
! Returns nzd complex values in array zd
!
! kwk debug:
! ***currently moves any input field component data in output zd, so this should only be used for single component data files ***


    use occam
    use mare2dem_input_data_params
    use em_constants
    use mare2dem_common, only: ismember

    implicit none

    include 'em_parameters.inc'

    integer, intent(in)  :: ind, ifreq
    logical, intent(in)  :: brx
    integer, intent(inout) :: nzd
    complex(8), dimension(nzd), intent(out) :: zd,zdm
    real(8),    dimension(nzd), intent(out) :: wzd

    integer :: i,j, irt, itr


    real(8) :: amp,  ampm, phs, phsm, zreal,zimag, zrealm,zimagm, std

    integer :: indAmp(12), indLogAmp(6), indPhs(6), indReal(6), indImag(6)

    indAmp = [  indAmpEx,indAmpEy,indAmpEz,indLog10AmpEx,indLog10AmpEy,indLog10AmpEz, &
    & indAmpBx,indAmpBy,indAmpBz,indLog10AmpBx,indLog10AmpBy,indLog10AmpBz ]

    indLogAmp = [  indLog10AmpEx,indLog10AmpEy,indLog10AmpEz, &
    & indLog10AmpBx,indLog10AmpBy,indLog10AmpBz ]

    indPhs    = [indPhsEx,indPhsEy,indPhsEz,indPhsBx,indPhsBy,indPhsBz]
    indReal   = [indRealEx,indRealEy,indRealEz,indRealBx,indRealBy,indRealBz ]
    indImag   = [indImagEx,indImagEy,indImagEz,indImagBx,indImagBy,indImagBz ]

    nzd = 0
    zd  = 0.
    zdm = 0.
    wzd = 0.

    if (brx) then
        irt = 4 ! use Rx column
        itr = 3
    else
        irt = 3 ! use Tx column
        itr = 4
    endif

    ! Loop through data and  (amp,phs) or (real,imag) pairs for request Rx (or Tx) and frequency:
    do i=1,nd

        if ( ( dp(i,irt)  == ind ) .and. ( dp(i,1) < 100 ) .and. ( dp(i,2) == ifreq ) ) then ! requested CSEM Rx (or Tx) and frequency

            ! Amplitude and phase:

            if (ismember(indAmp, dp(i,1) ) ) then

                amp  = d(i)
                ampm = dm(i)
                std  = sd(i)

                if (ismember(indLogAmp, dp(i,1) )) then
                    amp  = 10.**amp ! convert log10 to linear
                    ampm = 10.**ampm ! convert log10 to linear
                    std  = std/.4343*amp
                endif

                phs  = 0.
                phsm = 0.
                ! inner loop to find phase:
                do j=1,nd

                    if ( ( dp(j,irt)  == ind ) .and. ( dp(j,1) < 100 ) .and. ( dp(j,2) == ifreq ) .and. (dp(j,itr) == dp(i,itr) )) then ! requested CSEM Rx (or Tx) and frequency


                        if ( ismember( indPhs, dp(j,1) ) ) then
                            phs  = d(j)
                            phsm = dm(j)
                            exit
                        endif

                    endif
                enddo

                ! insert complex value:

                nzd = nzd + 1
                zd(nzd)  = amp*exp(IC*deg2rad*phs)
                zdm(nzd) = ampm*exp(IC*deg2rad*phsm)
                wzd(nzd) = std

                ! kwk debug:
                !write(*,*) nzd,zd(nzd),zdm(nzd),wzd(nzd)

                cycle ! to next in do i = 1,nc

            endif

            ! Real and imaginary:

            if ( ismember(indReal, dp(i,1) )) then

                zreal  = d(i)
                zrealm = dm(i)
                std    = sd(i)

                zimag  = 0.
                zimagm = 0.

                ! inner loop to find imaginary:
                do j=1,nd
                    if ( ( dp(j,irt)  == ind ) .and. ( dp(j,1) < 100 ) .and. &
                        ( dp(j,2) == ifreq )  .and. (dp(j,itr) == dp(i,itr) )  ) then ! requested CSEM Rx (or Tx) and frequency
                        if ( ismember( indImag, dp(j,1) ) ) then
                            zimag  = d(j)
                            zimagm = dm(j)
                            exit
                        endif

                    endif
                enddo

                ! insert complex value:
                nzd = nzd + 1
                zd(nzd)  = zreal + IC*zimag
                zdm(nzd) = zrealm + IC*zimagm
                wzd(nzd) = std

                cycle

            endif

        endif


    enddo


end subroutine get_complex_vector

!==================================================================================================================================!
!=============================================================================================================== apply_complex_coeff
!==================================================================================================================================!

subroutine apply_complex_coeff(ind,brx,ifreq,C)

    !
    ! Applies complex coefficient to model response using zm_correct = zm*C
    !
    ! where zm is the complex model response
    !

    use occam
    use mare2dem_input_data_params
    use em_constants
    use mare2dem_common, only: ismember

    implicit none

    include 'em_parameters.inc'

    integer, intent(in)  :: ind, ifreq
    logical, intent(in)  :: brx
    complex(8), intent(in) :: c

    integer     :: i,j,irt, itr

    real(8)     :: c_amp, c_phs, zreal,zimag
    complex(8)  :: zcmplx
    logical     :: bimag

    integer :: indAmp(12), indLogAmp(6), indPhs(6), indReal(6), indImag(6)

    indAmp = [  indAmpEx,indAmpEy,indAmpEz,indLog10AmpEx,indLog10AmpEy,indLog10AmpEz, &
    & indAmpBx,indAmpBy,indAmpBz,indLog10AmpBx,indLog10AmpBy,indLog10AmpBz ]

    indLogAmp = [  indLog10AmpEx,indLog10AmpEy,indLog10AmpEz, &
    & indLog10AmpBx,indLog10AmpBy,indLog10AmpBz ]

    indPhs    = [indPhsEx,indPhsEy,indPhsEz,indPhsBx,indPhsBy,indPhsBz]
    indReal   = [indRealEx,indRealEy,indRealEz,indRealBx,indRealBy,indRealBz ]
    indImag   = [indImagEx,indImagEy,indImagEz,indImagBx,indImagBy,indImagBz ]

    c_amp = abs(c)
    c_phs = rad2deg*ATAN2(aimag(c), dble(c))

    if (brx) then
        irt = 4 ! use Rx column
        itr = 3
    else
        irt = 3 ! use Tx column
        itr = 4
    endif


    ! Loop through data and  (amp,phs) or (real,imag) pairs for request Rx (or Tx) and frequency:
    do i=1,nd

        if ( ( dp(i,irt)  == ind ) .and. ( dp(i,1) < 100 ) .and. ( dp(i,2) == ifreq ) ) then ! requested CSEM Rx (or Tx) and frequency

            ! Amplitude:
            if ( ismember(indAmp, dp(i,1) ) ) then

                if ( ismember(indLogAmp, dp(i,1) )) then
                    dm(i) = log10( (10.**dm(i))*c_amp)
                else
                    dm(i) = dm(i)*c_amp
                endif

                cycle

            endif

            ! Phase:
            if (ismember(indPhs, dp(i,1) )) then
                dm(i) = dm(i)+c_phs ! kwk debug verify his!
                cycle
            endif

            ! Real and imaginary:
            if  ( ismember(indReal, dp(i,1) )) then

                zreal = dm(i)
                zimag = 0

                ! inner loop to find imaginary:
                bimag =.false.

                do j=1,nd

                    if ( ( dp(j,irt)  == ind ) .and. ( dp(j,1) < 100 ) .and. ( dp(j,2) == ifreq ) .and. (dp(j,itr) == dp(i,itr) )) then ! requested CSEM Rx (or Tx) and frequency

                        if (ismember( indImag, dp(j,1) ) ) then
                            zimag = dm(j)
                            bimag = .true.
                            exit
                        endif

                    endif
                enddo

                ! Apply correction to complex data type:
                zcmplx = cmplx(zreal,zimag)*c

                ! insert real and imaginary components back into dm:
                dm(i) = dble(zcmplx)
                if (bimag) dm(j) = aimag(zcmplx)

            endif

        endif

    enddo

end subroutine apply_complex_coeff


!==================================================================================================================================!
!================================================================================================== estimate_CSEM_timing_corrections
!==================================================================================================================================!
subroutine estimate_CSEM_timing_corrections

    use occam
    use mare2dem_input_data_params
    use em_constants

    implicit none

    if (allocated(iEstimateTxCorrection)) then

        if ( (any(iEstimateTxCorrection == 1)) .or. (any(iEstimateRxCorrection == 1)) ) then
            write(*,*) '  '
            write(*,*) ' Sorry, CSEM timing error correction estimates are not yet coded! '
            write(*,*) ' stopping... '
            stop
        endif

    endif


end subroutine estimate_CSEM_timing_corrections
