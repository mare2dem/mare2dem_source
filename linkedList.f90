!======================================================================! 
! Module of linked list routines and data types
!
! Modified from the examples in W. Brainerd's Fortran 2003 book
! 
! 
!    Kerry Key
!    Scripps Institution of Oceanography
!    kkey@ucsd.edu
!======================================================================! 
    
module linkedList
    
    implicit none 
    
    public :: insert, empty, delete, delete_list, print_list, get_length, get_values, get_indexes, get_first
    
    private :: insert_by_index, insert_by_value, insert_on_end
    
    type, public :: list 
        private
        type(node), pointer :: first => null() 
    end type list
    
    type, private :: node
        integer  :: index ! index of node  
        real(8)  :: value ! a data value associated with this node
        type(list) :: rest_of_list
    end type node
    
    
    integer, private :: ict
    contains 
    
    !-------------------------------------
    ! Insert:
    !-------------------------------------
    recursive subroutine insert(l, index, value, sortby)
    !
    ! sortby: 'index','value','none'
    !
    type(list), intent(in out) :: l 
    integer, intent(in) :: index 
    real(8), intent(in) :: value
    character(*), optional   :: sortby
    
    if (.not.present(sortby)) then
        call insert_on_end(l, index, value)
    else
       ! write(*,*) 'sortby is: ',sortby
        select case (trim(sortby))
            case ('index')
                call insert_by_index(l, index, value)  
            case ('value')            
                call insert_by_value(l, index, value)
            case ('none')            
                call insert_on_end(l, index, value)                
            case default   
                write(*,*) '!!!Error in module linkedList.f90 -> subroutine insert'
                write(*,*) ' Unrecognized value for parameter sortby: ' ,trim(sortby)
                write(*,*) ' Stopping!'
                stop
        end select
    endif
    end subroutine insert
    
    !-------------------------------------
    ! Insert on to end of stack:
    !-------------------------------------
    recursive subroutine insert_on_end(l, index, value)
    
    type(list), intent(in out) :: l 
    integer, intent(in) :: index 
    real(8), intent(in) :: value
    
    if (empty(l)) then
        allocate (l%first) 
        l%first%index = index
        l%first%value = value
    else
        call insert_on_end(l%first%rest_of_list, index, value) 
    end if
    
    end subroutine insert_on_end
     
    !-------------------------------------
    ! Insert sorted by index:
    !-------------------------------------
    recursive subroutine insert_by_index(l, index, value)
    
    type(list), intent(in out) :: l 
    integer, intent(in) :: index 
    real(8), intent(in) :: value
    type(list)          :: temp_list
    
    if (empty(l)) then
        allocate (l%first) 
        l%first%index = index
        l%first%value = value
    else if (index < l%first%index) then
        temp_list = l 
        allocate(l%first)
        l%first%index = index
        l%first%value = value
        l%first%rest_of_list = temp_list
    else if  (index == l%first%index) then   
        l%first%value = value ! override current value...?
    else
        call insert_by_index(l%first%rest_of_list, index, value) 
    end if
    
    end subroutine insert_by_index
       
    !-------------------------------------
    ! Insert sorted by value:
    !-------------------------------------
    recursive subroutine insert_by_value(l, index, value)
    
    type(list), intent(in out) :: l 
    integer, intent(in) :: index 
    real(8), intent(in) :: value
    type(list)          :: temp_list
        
    if (empty(l)) then
        allocate (l%first) 
        l%first%index = index
        l%first%value = value
    else if (value < l%first%value) then
        temp_list = l
        allocate(l%first)
        l%first%index = index
        l%first%value = value
        l%first%rest_of_list = temp_list
    else
        call insert_by_value(l%first%rest_of_list, index, value) 
    end if
    
    end subroutine insert_by_value
       
    !-------------------------------------
    ! Delete:
    !-------------------------------------
    recursive subroutine delete(l, index, found)
    
    type(list), intent(in out) :: l 
    integer, intent(in) :: index 
    logical, intent(out) :: found 
    type(list) :: temp_list
    
    if (empty(l)) then 
        found = .false.
    else if (l%first%index == index) then ! Delete node pointed to by l%first 
        temp_list = l 
        l = l%first%rest_of_list 
        deallocate(temp_list%first)
        found = .true. 
    else
        call delete(l%first%rest_of_list, index, found) 
    end if
    
    end subroutine delete
    
    !-------------------------------------
    ! Get Length:
    !-------------------------------------
    recursive subroutine get_length(l,l_length)
    
    type(list), intent(in out) :: l 
    integer, intent(in out)    :: l_length
     
    
    if (empty(l)) then 
        l_length = 0  ! past the last entry, now go back
        return
    else 
        call get_length(l%first%rest_of_list,l_length)
        l_length = l_length + 1
    end if
    
    end subroutine get_length

    !-------------------------------------
    ! Get Values
    !-------------------------------------
    recursive subroutine get_values(l,values)
    
    type(list), intent(in out) :: l 
    !integer, intent(in out)    :: ict 
    real(8), dimension(:),intent(inout) :: values 
     
    if (empty(l)) then 
        ict = 0 ! past the last entry, reset the counter
        return
    else
        ict = ict + 1
        values(ict) =  l%first%value  
        call get_values(l%first%rest_of_list,values)
        
    end if
    
    end subroutine get_values

    !-------------------------------------
    ! Get Indexes
    !-------------------------------------
    recursive subroutine get_indexes(l,indexes)
    
    type(list), intent(in out) :: l 
    !integer, intent(in out)    :: ict 
    integer, dimension(:),intent(inout) :: indexes
     
    if (empty(l)) then 
        ict = 0 ! past the last entry, reset the counter
        return
    else
        ict = ict + 1
        indexes(ict) =  l%first%index  
        call get_indexes(l%first%rest_of_list,indexes)
        
    end if
    
    end subroutine get_indexes
    
    !-------------------------------------
    ! Get First
    !-------------------------------------
    recursive subroutine get_first(l,value)
    
    type(list), intent(in out) :: l 
    real(8),intent(inout)      :: value 
     
    if (empty(l)) then 
        return
    else
        value =  l%first%value  
        
    end if
    
    end subroutine get_first
    
    !-------------------------------------
    ! Get Last
    !-------------------------------------
    recursive subroutine get_last(l,value)
    
    type(list), intent(in out) :: l 
    real(8),intent(inout)      :: value 
     
    if (empty(l)) then 
        return
    else
        value =  l%first%value  
        
    end if
    
    end subroutine get_last
        
    
    
    !-------------------------------------
    ! Is Empty?:
    !-------------------------------------
    function empty(l) result(empty_result)
    
    type(list), intent(in) :: l 
    logical :: empty_result 
    
    empty_result = .not. associated(l%first)
    
    end function empty

    !-------------------------------------
    ! Deallocate entire list:
    !-------------------------------------
    recursive subroutine delete_list(l)
    
    type(list), intent(in out) :: l 
    type(list) :: temp_list
    
    
    if (empty(l)) then 
        return 
    else  ! Delete node pointed to by l%first 
        temp_list = l 
        l = l%first%rest_of_list 
        deallocate(temp_list%first)
        call delete_list(l) 
    end if
    
    end subroutine delete_list
    

    !-------------------------------------
    ! Print:
    !-------------------------------------    
    recursive subroutine print_list(l)
    type(list), intent(in) :: l
        if (associated(l%first)) then 
            ict = ict + 1
            write(*,*) 'item#, index, value: ', ict, l%first%index , l%first%value
            call print_list(l%first%rest_of_list)
        else
            ict = 0
        endif
     
    end subroutine print_list
    
end module linkedList