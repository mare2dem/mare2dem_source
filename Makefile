# Makefile for MARE2DEM
#
# Usage:   make INCLUDE=<make.inc> 
#            
#   <make.inc> should be the name of an include (text) file that specifies the
#   MPI Fortran and C compilers and compilers flags for your computing system. I
#   recommend creating unique make.inc files (with specific names such as
#   mymegacluster.inc) for each system where you build the code. 
#
#   Example include files are provided in ./include/:
#
#    macos.inc    - MacOS system with the Intel oneAPI compiler suite.
#    habanero.inc - Linux cluster with the Intel parallel studio compilers.
# 
#   Note that this code currently requires the Intel Fortran and C compilers
#   and relies on the Intel Math Kernel Library (MKL). Either OpenMPI or MPICH
#   MPI implementations should work.
#                                            
#  Cleaning options:
#
#    make clean_all - This will remove all compiled codes, libraries and the
#                     executable, so that only the originally downloaded source
#                     files are present. You will need to do a make clean_all
#                     before compiling the code again if you update the
#                     compilers, update the MPI library etc.   
# 
#    make clean    - cleans only the object files but none of the included
#                    libraries. This option should only be used by developers
#                    adding new features to the base codes.
#  
#------------------------------------------------------------------------------

#---------------------------------------------------------------------
# Check for input INCLUDE file argument:
#----------------------------------------------------------------------

INC_FILE := $(shell echo $(INCLUDE) | tr A-Z a-z)

ifndef INCLUDE
    MARE2DEM: display_error_no_include display_usage;
else ifeq (,$(wildcard $(INC_FILE)))  
    MARE2DEM: display_error_include_not_found display_usage; 
else 
	MACOSCODESIGN= 
    include $(INC_FILE)
    MARE2DEM: disp build_mare2dem build_mare2dem_lib;
	@printf "#\n#\n# All done!  \n#\n#\n" 
endif


#-------------------------------------------------------------------------------
# Libraries required by MARE2DEM:
#-------------------------------------------------------------------------------
 
# 
# ScaLAPACK Library
#
SCALAPACK_dir =./libraries/scalapack-2.2.0
LIBSCALAPACK  = $(SCALAPACK_dir)/libscalapack.a

#-------------------------------------------------------------------------------
# Recipes:
#-------------------------------------------------------------------------------
 
# 
# Error message if INCLUDE variable not defined on input:
#
display_error_no_include:   
	@printf "\n\n\n !!!    Error making MARE2DEM     !!!   \n\n";
	@printf "    INCLUDE argument not given \n\n";

display_error_include_not_found: 
	@printf "\n\n\n !!!    Error making MARE2DEM     !!!   \n\n";
	@printf "    INCLUDE file not found:       %s \n\n" $(INC_FILE) ;
	@printf "    Did you specify the correct file path and name? \n\n";

display_usage:	 
	@printf "    Usage:   make INCLUDE=<make.inc>        \n\n";
	@printf "      <make.inc> should be the name of an include (text) file that specifies the\n";
	@printf "      MPI Fortran and C compilers and compilers flags for your computing system. I\n";
	@printf "      recommend creating unique make.inc files (with specific names such as\n";
	@printf "      mymegacluster.inc) for each system where you build the code. \n";
	@printf "      \n";
	@printf "      Example include files are provided in ./include/:\n";
	@printf "      \n";
	@printf "       macos.inc    - MacOS system with the Intel oneAPI compiler suite.\n";
	@printf "       habanero.inc - Linux cluster with the Intel parallel studio compilers.\n";
	@printf "      \n";
	@printf "      Note that this code currently requires the Intel Fortran and C compilers\n";
	@printf "      and relies on the Intel Math Kernel Library (MKL). Either OpenMPI or MPICH\n";
	@printf "      MPI implementations should work. \n\n\n";

# 
#  Cleaning functions:
# 
clean: clean_base

clean_all: clean_base clean_scalapack  

clean_base:
	@printf "\n#\n# cleaning MARE2DEM base ...\n#\n\n"; 
	$(RM) $(wildcard *.o)
	$(RM) $(wildcard *.mod)
	$(RM) $(wildcard *__genmod.f90)
	$(RM) MARE2DEM;
	@printf "\n# All clean \n"; 

clean_scalapack:
	@printf "#\n#\n# cleaning ScaLAPACK Library ... \n#\n#\n"; \
	cd $(SCALAPACK_dir); pwd; make clean;  			
 
#
# ScaLAPACK library build:
#
$(LIBSCALAPACK):
	@printf "#\n#\n# making ScaLAPACK library ... \n#\n#\n"; \
	cd $(SCALAPACK_dir); \
	make lib CC=$(CC) CCFLAGS='$(CFLAGS)' \
	FC=$(FC) FCFLAGS='$(FFLAGS)' \
	ARCH=$(ARCH) ARCHFLAGS=$(ARCHFLAGS) RANLIB=$(RANLIB); cd $(CURDIR); 

# 
# Triangle build:
# 
TRILIBDEFS = -DTRILIBRARY    

triangle.o:  triangle.c  triangle.h
	$(CC) $(TRILIBDEFS) $(TRICOPTS) -c -o $(BIN)triangle.o triangle.c

# 
# MARE2DEM build:
# 

disp:   
	@printf "#\n#\n# Making MARE2DEM and its dependencies... \n#\n#\n"

mare2dem_core = em_constants.o kdtree2.o fem2d_utilities.o  binarytree.o linkedList.o call_triangle.o sort.o \
			  intelmkl_solver.o quad.o\
			  string_helpers.o triangle.o mt1d.o kx_io.o em2dkx.o dc2dkx.o mare2dem_scalapack.o occam.o\
			  c_fortran_triangle.o filtermodules.o   \
			  mare2dem_common.o  spline_kx_module.o mare2dem_worker.o \
			  mare2dem_mpi.o mare2dem_penaltymatrix.o  mare2dem_io.o  em2d.o
		      
mare2dem_exe = $(mare2dem_core) runmare2dem.o 

mare2dem_lib = $(mare2dem_core)  mare2dem_lib_interface.o

build_mare2dem: $(LIBSCALAPACK) $(mare2dem_exe) 
	       	 	$(FC) $(FFLAGS) $(mare2dem_exe) $(LIBSCALAPACK) $(MKLLIB) -o MARE2DEM
            	ifneq ($(strip $(MACOSCODESIGN)),)
		 			@printf "#\n# signing MacOS executable...\n"
		 			@eval $(MACOSCODESIGN)
    			endif 

build_mare2dem_lib:	$(LIBSCALAPACK) $(mare2dem_lib) 
					@printf "#\n#\n# Making MARE2DEM library...  \n#\n#\n"
					$(FC) $(FFLAGS) $(mare2dem_lib) $(LIBSCALAPACK) $(MKLLIB) -shared -o mare2dem_lib.o

# 
# Test build:
# 
Test_Files	= binaryTree.o call_triangle.o triangle.o  c_fortran_triangle.o  \
		      TestForSlivers.o  
		
TestForSlivers:	 $(Test_Files) 
	$(FC) $(FFLAGS) $(Test_Files) -o TestForSlivers
	
test_lib_files = $(mare2dem_lib) test_library.o

test_library:  $(LIBSCALAPACK)  $(test_lib_files) 
	$(FC) $(FFLAGS) $(test_lib_files)  $(LIBSCALAPACK) $(MKLLIB) -o test_library
	

#-------------------------------------------------------------------------------
# Compiling recipes:
#-------------------------------------------------------------------------------
#
# General Fortran compile:
%.o: %.f90 
	$(FC) $(FFLAGS) -c -o $@ $^

# General C compile:
%.o : %.c
	$(CC) $(CFLAGS) $(CDEFS) -c -o $@ $< 


