 program test_library 
!
! utility for testing the mare2dem forward library that can be linked from external codes
!
! build notes:  in mare2dem_source:  "make INCLUDE=./include/macos.inc" to make MARE2DEM and mare2dem_lib.o
!                                      then "make INCLUDE=./include/macos.inc test_library" to make this test program
!                                      then run in mare2dem_examples/Demo/inversion_MT with "mpirun -n 4 ../test_library"
!
! Tests the following subroutines in mare2dem_lib_interface.f90:
!
!   mare2dem_load_files()          - Loads a resistivity file into mare2dem's memory,
!                                    returns number of free regions and number of parameters per region (e.g., for isotropic = 2,
!                                    tiz anisotropy = 2, etc).
!                                    Should only be called on rank 0 processor (for a given local communicator)
!
!   mare2dem_get_params()          - Returns the log10 resistivity parameters in each free parameter region (length = #regions x 
!                                    #params_per_region)
!                                    Also returns location of parameter centroids.
!
!   mare2dem_get_data()            - Returns the data and standard error arrays.
!
!   mare2dem_forward()             - Computes forward model response for model loaded with mare2dem_load_files. You must pass in 
!                                    log10resitivity array to use for the free parameters.
!
!
!   mare2dem_inversion()           - Computes inversion of input resistivity parameters and data array. Uses model geometry, 
!                                    data uncertainty and other MARE2DEM parameters read in by mare2dem_load_files().
!
!

! For Julia interface,  use mare2dem_load_files to load the files and get the number of free parameter regions. 
! Then you can allocate memory for them in julia an initialize the arrays, then use mare2dem_get_params to return the
!  arrays for the free parameter log10 resistivity and the free parameter region centroids.  After calling mare2dem_load_files()
! you can use mare2dem_forward() or mare2dem_inversion().
!
!
!
 
	use mare2dem_lib_interface
	use mpi
	
	use, intrinsic :: iso_fortran_env, only: int64, real64
    use, intrinsic :: iso_c_binding, only: c_char
    	
	implicit none
	 

    ! Derived type for dealing with annoying differences between Fortran and C  character arrays 
    type :: c_char_str
         integer(int64)                 :: n
         character(len=1,kind=c_char)   :: str(256)         
    end type
     
    ! Now use the derived type for these file names: 
    type(c_char_str)    :: inputFile, saveFile
    integer             :: rank,ierr,nproc,i, np
    character(256)      :: str
    integer(int64)      :: nFreeRegions=0,nParamsPerRegion=0, numdata=0,numrows_pen=0,numnz_pen=0
    integer(int64)      :: nIter, saveNum,do_work_flag,bQuiet
    integer(int64)      :: inv_method, nproc_per_team, nStartingIteration,niterations
    integer(int64)      :: iSaveLogfile,iSaveModel,iSaveResp,convergenceFlagOut
    real(real64)        :: log10Mu, chiSquaredMisfit,totaltime
    integer(int64), dimension(:),   allocatable  :: pen_row,pen_col 
    real(real64),   dimension(:),   allocatable  :: nLog10RhoFree, nLog10RhoPrej, nData, nUnc, nResp, pen_val  
    real(real64),   dimension(:,:), allocatable  :: freeCentroids   ! nFreeREgions x 2 free region centroids (y,z)
 
    

!
! Initialize MPI:   
!
    call mpi_init( ierr )
    call mpi_comm_rank( MPI_COMM_WORLD, rank, ierr )	 ! here mcomm defined to be mpi_world in mare2dem_mpi, but could be passed in instead
    call mpi_comm_size( MPI_COMM_WORLD, nproc, ierr )  
    
!
! Set base name of model files to read in: <basename.0>.resistivity and <basename>.poly 
!
! Since library interface is set to be called from C codes (or Julia), we can't use Fortran strings
! and instead must have an array of single c_chars. This is clunky, but seems to be the only
! way to have interoperability with string passing between C and Fortran:
!
 
    str = 'Demo.0'
    call f_str_to_c_char(str,inputFile%n,inputFile%str)    ! converts fortran string (character(256)) to c_char array and returns length nstr 

    bQuiet  = 1 ! set to > 0 to tell MARE2DEM to be quiet

!-----------------------------------------------------------------------------------------------------------------------------------
! Test 1: load MARE2DEM input files into memory only on rank 0 processor (mare2dem_forward() will dole out data to worker processors)
!-----------------------------------------------------------------------------------------------------------------------------------
 
    if (rank == 0)	then  ! only need to load files on manager processor
            
        call mare2dem_load_files(inputFile%n,inputFile%str,nFreeRegions,nParamsPerRegion,numdata,numrows_pen,numnz_pen,bQuiet)  
        
	    write(*,*) '' 
	    write(*,*) '----------------------------------------------------------------' 
	    write(*,*) '----------------------------------------------------------------' 
	    write(*,*) 'Test 1: loaded MARE2DEM files for: ', (inputFile%str(i),i=1,inputFile%n) 
	    write(*,'(a32,g0)') 'nFreeRegions: ',nFreeRegions
	    write(*,'(a32,g0)') 'nParamsPerRegion: ',nParamsPerRegion 
	    write(*,'(a32,g0)') '# of data: ',numdata 
	    write(*,'(a32,g0)') '# of row in penalty mtx: ',numrows_pen 
	    write(*,'(a32,g0)') '# of non-zeros in penalty mtx: ',numnz_pen 
	    write(*,*) '' 
	     
    endif
    call mpi_barrier(MPI_COMM_WORLD,ierr)
    
!-----------------------------------------------------------------------------------------------------------------------------------
! Test 2: get free parameters and parameter centroids:
!-----------------------------------------------------------------------------------------------------------------------------------

    allocate( nData(numdata),nUnc(numdata))
    allocate( pen_row(numnz_pen),pen_col(numnz_pen),pen_val(numnz_pen))
      
    if (rank == 0) then
    
        allocate(nLog10RhoFree(nFreeRegions*nParamsPerRegion),freeCentroids(nFreeRegions*nParamsPerRegion,2))
        
        write(*,*) '' 
        write(*,*) '----------------------------------------------------------------' 
        write(*,*) '----------------------------------------------------------------' 
        write(*,*) 'Test 2a: reading in free parameters and centroids: ' 
        write(*,*) ' '
        
        call mare2dem_get_params(nLog10RhoFree,freeCentroids,nFreeRegions*nParamsPerRegion)   
        
        write(*,*)    '10**nLog10RhoFree(1:10):',    (10**nLog10RhoFree(i),i=1,10) 	 
        write(*,*)    'First 10 centroids:' 
        do i=1,10
            write(*,*) freeCentroids(i,1),freeCentroids(i,2)
        enddo        
        
        ! Test retrieving the data and uncertainty arrays:
        write(*,*) '' 
	    write(*,*) 'Test 2b: receiving data and uncertainty arrays: ' 
	    write(*,*) ''
	    
      
        call mare2dem_get_data(nData,nUnc,numdata)
            
        write(*,*)    'First 10 data and uncertainty values:' 
        do i=1,10
            write(*,*) nData(i),nUnc(i)
        enddo             
            
        ! Test retrieving the penalty matrix:
        write(*,*) '' 
	    write(*,*) 'Test 2c: receiving sparse penalty matrix arrays: ' 
	    write(*,*) ''
	    call mare2dem_get_penaltymatrix(pen_row,pen_col,pen_val,numnz_pen) 
      
       
        write(*,*)    'First 10 penalty matrix entries [i,j,val]' 
        do i=1,10
            write(*,*) pen_row(i),pen_col(i),pen_val(i)
        enddo            
            
   
	    ! Test routine that saves resistivity array to a <name>.<niter>.resistivity file:
	    
        str = 'testSave'
        call f_str_to_c_char(str,saveFile%n,saveFile%str)  
 	    chiSquaredMisfit = 0.1234  ! dummy value for testing
	    nIter = 2
	        
	    write(*,*) '' 
	    write(*,*) 'Test 2d: saving parameters to file: ',(saveFile%str(i),i=1,saveFile%n) 
	    write(*,*) ''
	   
	    call mare2dem_save_resistivity(saveFile%n,saveFile%str,nLog10RhoFree,chiSquaredMisfit,nIter) 
	    
	    deallocate(nLog10RhoFree,freeCentroids)     
	   	   
    endif
    
    call mpi_barrier(MPI_COMM_WORLD,ierr)
  
         
!-----------------------------------------------------------------------------------------------------------------------------------
! Test 3: Forward Call 
!-----------------------------------------------------------------------------------------------------------------------------------

    nproc_per_team = nproc  ! for testing, use only one team consisting of all processors. For Dan's PT code, each PT chain will have 
                            ! some number of processors set asside for MARE2DEM forward calls (like nproc_per_team = 4 for 6 PT 
                            ! chains on a 24-core habanero node)

    if (rank == 0) then
        write(*,*) ' '
        write(*,*) '----------------------------------------------------------------' 
        write(*,*) '----------------------------------------------------------------' 
        write(*,*) 'Test 3: calling mare2dem_forward ' 
        write(*,*) ' '
    endif  

    allocate(nLog10RhoFree(nFreeRegions*nParamsPerRegion))
    nLog10RhoFree   = 0  ! set entire array of free parameters to be 0 = log10 ( 1 ohm-m).        
    do_work_flag    = 1  
    saveNum         = 999 ! dummy value for testing. Saves to {saveFile}.saveNum.resp
    
    str = 'test_library_fwd_output'
    call f_str_to_c_char(str,saveFile%n,saveFile%str)  ! converts fortran string (character(256)) to c_char array and returns length nstr 
    
    allocate(nResp(numdata))
    
    
    call mare2dem_forward(nproc_per_team,nLog10RhoFree,chiSquaredMisfit,saveFile%n,saveFile%str,saveNum,nResp,bQuiet) 
 
    if (rank == 0) then
        write(*,*) ' after mare2dem_forward(), chiSquaredMisfit: ', chiSquaredMisfit
        write(*,*) ' after mare2dem_forward(), rms: ', sqrt(chiSquaredMisfit/numdata)
        write(*,*) ''        
        !display some results too:
        do i = 1,10
            write(*,'(a,g9.2,a,g9.2)')    ' nData: ', nData(i), ' resp: ', nResp(i)
        enddo
        write(*,*) ''        
    endif
    if (allocated(nLog10RhoFree))  deallocate(nLog10RhoFree) 
    if (allocated(nResp))         deallocate(nResp) 
      
!
! Notes: Steps 1 and 2 above could be called by the local rank 0 processor on startup of the Julia code.
!        Then Test 3 will be called for each forward evaluation and is called by all processors. nproc_per_team is used to determine 
!        the local MPI communicator team. nLog10RhoFree only needs to be defined on the team's rank=0 process since in MARE2DEM it 
!        passed the resistivity array to the workers.
!    
 
    call mpi_barrier(MPI_COMM_WORLD,ierr)
 
!-----------------------------------------------------------------------------------------------------------------------------------
! Test 4:  inversion call
!-----------------------------------------------------------------------------------------------------------------------------------
! 
! Note that arrays nLog10RhoFree,nLog10RhoPrej, nData, nResp only need to have size > 0 on rank 0 processor
! since for mare2dem_inverse does I/O only on manager (rank=0). Still need to allocate to size 0 on other
! processors so that compiler doesn't throw error message about unallocated arrays input to mare2dem_inversion() call.
!    
    
    np = nFreeRegions*nParamsPerRegion
    allocate(nLog10RhoFree(np),nLog10RhoPrej(np))
    allocate(nResp(numdata))
    
        
    if (rank == 0) then
        write(*,*) ' '
        write(*,*) '----------------------------------------------------------------' 
        write(*,*) '----------------------------------------------------------------' 
        write(*,*) 'Test 4: calling mare2dem_inversion ' 
        write(*,*) ' '
    endif
    
    inv_method          = 1    ! 0 = occam, 1 = fixed mu gauss-newton 
    log10Mu             = 1    ! staring value for log10 mu. Normal Occam starts with log10Mu = 5. 
    nLog10RhoFree       = 0.5  ! set entire array of free parameters to be 0 = log10 ( 1 ohm-m).
    nLog10RhoPrej       = 0  ! prejudice model is 0 too.  
    nStartingIteration  = 0  ! starting iteration number, appended to sFileName    
    iSaveLogfile        = 1  ! 1 = save inversion logfile
    iSaveModel          = 2  ! 1 = save only final iteration model, 2 = save every inversion iteration 
    iSaveResp           = 2  ! 1 = save only final iteration response, 2 = save every inversion iteration 
 
    str = 'test_library_inv_output'
    call f_str_to_c_char(str,saveFile%n,saveFile%str)  ! converts fortran string (character(256)) to c_char array and returns length nstr 
     
    call  mare2dem_inversion(inv_method,nproc_per_team,log10Mu,&
                                 & nLog10RhoFree,nLog10RhoPrej, & 
                                 & nData, nResp, & 
                                 & nStartingIteration, &
                                 & saveFile%n,saveFile%str,iSaveLogfile,iSaveModel,iSaveResp, bQuiet, &
                                 & chiSquaredMisfit, convergenceFlagOut,niterations,totaltime ) 
                                 
                                    
    if (rank == 0) then
        write(*,'(a,g0,a,g0)') ' back from mare2dem_inversion, chiSquaredMisfit: ', chiSquaredMisfit, ' convergenceFlag: ',convergenceFlagOut
        
        !display some results too:
        do i = 1,10
            write(*,'(a,g9.2,a,g9.2,a,g9.2)')    'rho: ',10**nLog10RhoFree(i),' nData: ', nData(i), ' resp: ', nResp(i)
        enddo
        write(*,*) ''
    endif
     
    if (allocated(nLog10RhoFree))   deallocate(nLog10RhoFree)  
    if (allocated(nLog10RhoPrej))   deallocate(nLog10RhoPrej)  
    if (allocated(nData))           deallocate(nData)  
    if (allocated(nResp))           deallocate(nResp)      
    if (allocated(pen_row))         deallocate(pen_row, pen_col, pen_val)      
 
!-----------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------

!
! Finalize MPI
!    
    call mpi_barrier(MPI_COMM_WORLD,ierr)
    call mpi_finalize(ierr)


end program test_library
 