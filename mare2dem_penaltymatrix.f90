!
!    Copyright 2020-2022
!    Kerry Key
!    Lamont-Doherty Earth Observatory
!    kkey@ldeo.columbia.edu
!
!    This file is part of MARE2DEM.
!
!    MARE2DEM is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    MARE2DEM is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MARE2DEM.  If not, see <http://www.gnu.org/licenses/>.
!
!-----------------------------------------------------------------------

    module  mare2dem_penaltymatrix
!
! This module is new as of November 2020 and replaces the penalty matrix routines in the Mamba2D.m MATLAB code. 
! Now the penalty matrix is generated on the fly when MARE2DEM is launched rather than being a required input file.
! All input settings for generating the penalty matrix are specified in the .resistivity file, with the benefit
! that the various roughness penalty settings are stored there for posterity, and more importantly for inversion
! reproducibility. Previously, the roughness penalty settings were only visible in the Mamba2D.m gui edit boxes. 
! 
! Currently, this routine supports: spatial roughness penalty, penalty against anisotropy and anisotropy ratio 
! roughness penalty.
!    
    
    use occam     ! module defines penalty matrix structure and holds type(PenaltyMatrix), public:: pen 
    use mare2dem_mpi
    
    implicit none

    ! penalty related parameters can be modified by lines added to the input .resistivity file:    
    character(80)       :: cRoughnessPenalty                ='gradient'   ! 'gradient' or 'first_difference'
    real(RealPrec)      :: yzPenaltyWeights(2)              = [3.d0,1.d0]  
    real(RealPrec)      :: penaltyCutWeight                 = 0.1d0   
    real(RealPrec)      :: anisotropyPenaltyWeight          = 0.d0  
    real(RealPrec)      :: anisotropyRatioRoughnessWeight   = 1d0        
           
               
    contains
    
!-----------------------------------------------------------------------------------------------------------------------------------        
!-------------------------------------------------------------------------------------------------------------generate_PenaltyMatrix        
!-----------------------------------------------------------------------------------------------------------------------------------          
    subroutine generate_PenaltyMatrix(inmodel)
    
!
! Creates penalty matrix in compressed sparse row format.
! 
 
    use fem2d_utilities , only: get_abc_coeffs ! for element geometry functions
    use kx_io           , only: iFreeParam,cAnisotropy
    use triangle_mesh   ! for meshing functions
    use mare2dem_global 
    use linkedList      ! for temporary storage of region adjacency lists 
    
    use mare2dem_common, only : print2col32
    
    
    ! input arguments:
    type(trimesh), intent(in)            :: inmodel
    
    ! local variables:
    
    integer         :: neighborOrder = 1 ! (dflt=1). 0 uses edge neighbors only, 1 uses node neighbors (includes more neighs than edges) 
    character(32)   :: tristr
    type(trimesh)   :: mesh
              
    type(list),     dimension(:),       allocatable :: regionAdjList, vert2tri, tri2tri
    real(RealPrec), dimension(:,:),     allocatable :: centroids,sumAreaCentroid
    real(RealPrec), dimension(:,:,:),   allocatable :: cutSegments
    real(RealPrec), dimension(:),       allocatable :: area, values  
    integer,        dimension(:),       allocatable :: indexes
    
    integer                         :: e,i,j,k,lt,n(3), iregion,neighs(3),iregion_neigh,n1,n2,nCuts
    integer                         :: ict, iPass, iregion1,iregion2, ind1,ind2,iFree1,iFree2
    integer                         :: ind1_a,ind2_a,iFree1_a,iFree2_a
    integer                         :: ind1_b,ind2_b,iFree1_b,iFree2_b
    integer                         :: nspatial, naniso, nratiospatial, eps_aniso(6) 
    real(RealPrec), dimension(3)    :: ye,ze,a,b,c    
    real(RealPrec)                  :: eleArea, sumAreaNeighs,dy,dz,wy,wz,dr,weight,wt,segTest(2,2), cut_wt
    logical                         :: lFree, lIntersect
    
    

!
! Overview of implementation:
!
! 1) Create triangular mesh of input model for facilitating geometry traversal
! 2) Compute centroids of each region of input model (regions are enclosed by the input segments)
! 3) Generate array of any segments marked for roughness penalty cuts
! 4) Generate region adjacency list of edge neighbors or edge+node neighbors. For adjacent
!    regions, see if centroid-centroid segment crosses penalty cut segments and set penalty cut weight if so.
! 5) Generate penalty matrix for a) spatial roughness penalty, b) anisotropy penalty and c) anisotropy ratio spatial roughness
!    penalty
!

    if (allocated(pen%colind)) return ! penalty matrix already set so must have been input as file using old format

    ! 
    ! Print penalty matrix settings:
    !
    if (lprintData) then 
        write(*,*) '======== Generating Penalty Matrix For Inversion ========================='
        write(*,*) ' '    
        !kwk debug add penalty matrix settings print out here       
                
    endif
    

    allocate(indexes(1000),values(1000))
    
   !--------------------------------------------------------------------------------------------------------
   ! Generate triangular mesh of input model using sparsest possible constrained Delaunay triangulation:
   !--------------------------------------------------------------------------------------------------------
             
    tristr = 'q0QpanjA'//CHAR(0)
    call call_triangle(tristr,inputmodel,mesh)  

    if (inputmodel%numberofregions /= mesh%numberofregions) then
        write(*,*)' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        write(*,*) ' Error in generate_PenaltyMatrix: inputmodel%numberofregions /= mesh%numberofregions! '
        write(*,*)' !!!!!!!!!!!!!!!!!!!!!!!!!!!! stopping        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        call exitMARE2DEM
    endif
 
 
    !-------------------------------------------------------
    !  Get the centroids for all regions in the model:
    !-------------------------------------------------------
    allocate(centroids(2,mesh%numberofregions), sumAreaCentroid(2,mesh%numberofregions),area(mesh%numberofregions) )
    
    centroids       = 0d0 
    sumAreaCentroid = 0d0
    area            = 0d0
    
    do e = 1,mesh%nele
        
        iregion =  nint(abs(mesh%attr(e)))

        ! get verts and area:        
        n    = mesh%emap(1:3,e)
        ye   = mesh%y(n)
        ze   = mesh%z(n)   
        call get_abc_coeffs(ye,ze,a,b,c,eleArea)   
                
        area(iregion)           = area(iregion) + eleArea
        sumAreaCentroid(1,iregion) = sumAreaCentroid(1,iregion)  + eleArea*sum(ye)/3.
        sumAreaCentroid(2,iregion) = sumAreaCentroid(2,iregion)  + eleArea*sum(ze)/3. 
                
!         write(*,'(i10,1x,f16.1,1x,3(f18.1,1x))') e,eleArea,xyz
!         write(*,*) iregion, sumAreaCentroid(:iregion) 
                             
    enddo
    
    do iregion = 1,mesh%numberofregions
        centroids(:,iregion) = sumAreaCentroid(:,iregion) / area(iregion)
        !write(*,'(i10,1x,3(es12.1,1x),g12.1)') iregion,centroids(:,iregion), area(iregion)
    enddo
    
    deallocate(sumAreaCentroid)
       
    !-------------------------------------------------------
    ! Generate array of any penalty cut segments:
    !-------------------------------------------------------
    
    do iPass = 1,2 ! 1st pass counts, 2nd pass allocates and inserts
    
        if (iPass == 2) then
            if (ict > 0 ) allocate(cutSegments(2,2,ict))
        endif
        
        ict = 0
         
        do i = 1,inputmodel%numberofsegments  ! inmodel has all input model segments prior to splitting to make mesh%,
                                              ! so can have fewer segments than mesh does, so faster for searches
                                                
            if (inmodel%segmentmarkerlist(i) < 0 ) then
                
                ict = ict + 1
                if (iPass == 2) then ! 
                    n1 = inputmodel%segmentlist(2*i-1)
                    n2 = inputmodel%segmentlist(2*i  )
                    cutSegments(1,1,ict) = inmodel%y(n1)
                    cutSegments(2,1,ict) = inmodel%z(n1)
                    cutSegments(1,2,ict) = inmodel%y(n2)
                    cutSegments(2,2,ict) = inmodel%z(n2)
                endif 
        
            endif                                    
                                              
        enddo
     
    enddo    
    nCuts = ict
    
    !-------------------------------------------------------
    !  Get region adjacency list
    !-------------------------------------------------------
    
    ! Allocate storage:
    allocate(regionAdjList(mesh%numberofregions))   
    
    !      
    ! Edge neighbor adjacency only:
    !   
    if (neighborOrder == 0 ) then ! face neighbors only:
    
        do e = 1,mesh%nele
            neighs  = mesh%neighborlist(:,e)   ! three neighboring elements     
            iregion =  nint(abs(mesh%attr(e)))
            do j = 1,3
                if ( neighs(j) > 0 ) then ! neighbor exists
                    iregion_neigh = nint(abs(mesh%attr(neighs(j))))        
                    
                    if (iregion /= iregion_neigh)  then
                    
                        if (       any( iFreeParam( (iregion-1)*nRhoPerRegion+[1:nRhoPerRegion]) > 0) &
                             .and. any( iFreeParam( (iregion_neigh-1)*nRhoPerRegion+[1:nRhoPerRegion]) > 0 ) ) then
                         
                            wt = 1d0 
                    
                            if (nCuts > 0) then
                    
                                segTest(:,1) =  centroids(:,iregion) 
                                segTest(:,2) =  centroids(:,iregion_neigh) 
                        
                                lIntersect = intersectionTest(segTest,cutSegments)
         
                                if (lIntersect) wt = penaltyCutWeight

                            endif
                            
                            call insert(regionAdjList(iregion), iregion_neigh, wt, 'index')

                        endif
                    

                    endif
                endif   
            enddo
        enddo
    
    elseif ( neighborOrder == 1)  then! node neighbors
    
        ! First create vert to tet list of all tets that use a given vertex, then use that list to 
        ! create a tet to tet list for all tets that are vertex neighbors. Finally, use the tet to 
        ! tet list to find adjacent regions that share vertices.
        allocate( vert2tri(mesh%nnod), tri2tri(mesh%nele) )
        
        ! make vert2tri:
        do e = 1,mesh%nele
             n    = mesh%emap(1:3,e)
            do j = 1,3
                call insert(vert2tri(n(j)), e, 1d0, 'index')
            enddo    
        enddo
        
        ! make tri2tri:
        do e = 1,mesh%nele
            n    = mesh%emap(1:3,e)
            do j = 1,3
                k = n(j)
                ! get all tets attached to vertex k:
                call get_length(vert2tri(k),lt)
                if (lt > size(indexes,1)) then
                    write(*,*) 'error in generate_PenaltyMatrix, lt > size(indexes,1) for tri2tri assembly!'
                    cycle            
                endif
                call get_indexes(vert2tri(k),indexes)
                ! add them to tri2tri for tet e:
                do i = 1,lt
                    call insert(tri2tri(e),indexes(i), 1d0, 'index')   
                enddo
            enddo           
        enddo
        
        ! now use tri2tri to make regionAdjList:
        do e = 1,mesh%nele
            call get_length(tri2tri(e),lt)
            if (lt > size(indexes,1)) then
                write(*,*) 'error in generate_PenaltyMatrix, lt > size(indexes,1) for tri2tri usage!'
                cycle 
            endif
            call get_indexes(tri2tri(e),indexes)
            iregion =  nint(abs(mesh%attr(e))) 
            do i = 1,lt
                iregion_neigh = nint(abs(mesh%attr(indexes(i))))
                if (iregion_neigh /= iregion) then
                
                    if (       any( iFreeParam( (iregion-1)*nRhoPerRegion+[1:nRhoPerRegion]) > 0) &
                         .and. any( iFreeParam( (iregion_neigh-1)*nRhoPerRegion+[1:nRhoPerRegion]) > 0 ) ) then
 

                        wt = 1d0 
                        if (nCuts > 0) then

                            segTest(:,1) =  centroids(:,iregion) 
                            segTest(:,2) =  centroids(:,iregion_neigh) 
    
                            lIntersect = intersectionTest(segTest,cutSegments)
    
                            if (lIntersect) wt = penaltyCutWeight
                        
                        endif
                                           
                        call insert(regionAdjList(iregion), iregion_neigh, wt, 'index')   
                    
                    endif ! both regions have at least one free param
                    
                endif    
            enddo                    
        enddo
        deallocate( vert2tri, tri2tri )
    endif      

    !-------------------------------------------------------
    !  Now generate penalty matrix:
    !-------------------------------------------------------  

    eps_aniso  = 1
    do i = 1,2 ! make two copies of [1,2,3] or [1,2] for cyclic aniso component indices
        do j = 1,nRhoPerRegion
            eps_aniso( (i-1)*nRhoPerRegion + j) = j
        enddo
    enddo
    
    do iPass = 1,2
        
        if (iPass == 2) then ! allocate penalty matrix compressed sparse structure:
            pen%nrows = ict
            !pen%nnz   = 2*nspatial + 4*nratiospatial + 2*naniso      
            !write(*,*) 'ict, nspatial, nratiospatial, naniso, pen%nnz :',ict, nspatial, nratiospatial, naniso, pen%nnz
            allocate(pen%colind(pen%nnz),pen%val(pen%nnz),pen%rowptr(ict+1))
        endif

        ict = 0
        nspatial        = 0
        nratiospatial   = 0    
        naniso          = 0 
        pen%nnz         = 0 
        
        !--------------------------------------------
        ! Spatial Roughness Penalty
        !--------------------------------------------
    
        do iregion1 = 1,nRegions
                    
            ! get neighboring regions from regionAdjList:
            call get_length( regionAdjList(iregion1),lt)
            if (lt > size(indexes,1)) cycle ! kwk debug: avoid outer region that overlaps everything? 
            call get_indexes(regionAdjList(iregion1),indexes)
            call get_values(regionAdjList(iregion1),values)
                     
            ! get total area of all FREE neighbors:
            sumAreaNeighs = 0d0            
            do i = 1,lt
                iregion2 = indexes(i)
                lFree = .false.
                do j = 1,nRhoPerRegion
                    ind2 = (iregion2-1)*nRhoPerRegion+j
                    if (iFreeParam(ind2)>0) lFree = .true.      
                enddo
                if (lFree) sumAreaNeighs = sumAreaNeighs + area(iregion2)  
            enddo
            
            do i = 1,lt ! loop over adjacent regions
            
                iregion2 = indexes(i)
                cut_wt   = values(i)     ! penalty cut weight
                
                do j = 1,nRhoPerRegion
    
                    ind1 = (iregion1-1)*nRhoPerRegion+j
                    ind2 = (iregion2-1)*nRhoPerRegion+j
    
                    iFree1 = iFreeParam(ind1)
                    iFree2 = iFreeParam(ind2)
    
                    if ( (iFree1 > 0).and.(iFree2 > 0) ) then ! both parameters are free, add a penalty term:
                    
                        ict = ict + 1 ! count penalty term
                        nspatial = nspatial + 1
                        
                        if (iPass == 2) then  ! Set the penalty matrix terms:
                        
                            select case (cRoughnessPenalty)

                            case ('gradient')  
                                
                                ! centroid-centroid distance:    
                                dy = abs(centroids(1,iregion1) - centroids(1,iregion2))
                                dz = abs(centroids(2,iregion1) - centroids(2,iregion2))
                  
                                wy = yzPenaltyWeights(1)
                                wz = yzPenaltyWeights(2)
               
                                dr = sqrt( (dy/wy)**2+(dz/wz)**2)
                            
                                weight = sqrt(area(iregion1))*sqrt(area(iregion2)/sumAreaNeighs)/dr
                            
                            
                            case ('first_difference') ! no gradient or area weights, just vanilla 1st difference

                                weight = 1d0

                            end select                           

                            weight = weight*cut_wt 
                            
                            ! special case for 'tiz_ratio' parameterization. Apply anisotropyRatioRoughnessWeight if j==2:
                            if ( (cAnisotropy(1:9) == 'tiz_ratio') .and. (j == 2) ) then ! 2nd parameter is log10(z/h) ratio:
                                weight = weight*anisotropyRatioRoughnessWeight
                            endif
                            
                            pen%val(pen%nnz+1)    =  weight
                            pen%val(pen%nnz+2)    = -weight
                            pen%colind(pen%nnz+1) = iFree1
                            pen%colind(pen%nnz+2) = iFree2
                            pen%rowptr(ict)       = pen%nnz+1
                                                        
                        endif
                        
                        pen%nnz = pen%nnz + 2
                        
                    endif
    
                enddo ! j = 1,nRhoPerRegion
            enddo ! i = 1,lt ! loop over adjacent regions
        
        enddo ! iregion1 = 1,nRegions
        
        if (lprintData) then 
            if (iPass == 2) write(*,'(a48,1x,i0)') 'Number of spatial roughness penalty rows:', nspatial
        endif
                
        pen%nrows_spatial = ict  ! store this for applying R(m-m*) derived term on rhs of occam update equations, if requested
                
        !--------------------------------------------
        ! Anisotropy ratio roughness penalty (e.g. || R (m_v - m_h) || ):  
        !--------------------------------------------       
       
        if ( (nRhoPerRegion > 1)                        .and.  & !(cAnisotropy(1:9) .ne.'tiz_ratio')         .and.  &
             (cAnisotropy(1:17).ne.'isotropic_complex') .and.  & 
             (cAnisotropy(1:12).ne.'isotropic_ip')  ) then      
                    
            do iregion1 = 1,nRegions
                    
                ! get neighboring regions from regionAdjList:
                call get_length( regionAdjList(iregion1),lt)
                if (lt > size(indexes,1)) cycle ! kwk debug: avoid outer region that overlaps everything? 
                call get_indexes(regionAdjList(iregion1),indexes)
                call get_values(regionAdjList(iregion1),values)
         
            
                ! get total area of all FREE neighbors:
                sumAreaNeighs = 0d0            
                do i = 1,lt
                    iregion2 = indexes(i)
                    lFree = .false.
                    do j = 1,nRhoPerRegion
                        ind2 = (iregion2-1)*nRhoPerRegion+j
                        if (iFreeParam(ind2)>0) lFree = .true.      
                    enddo
                    if (lFree) sumAreaNeighs = sumAreaNeighs + area(iregion2)  
                enddo
            
                do i = 1,lt ! loop over adjacent regions
            
                    iregion2 = indexes(i)
                    cut_wt   = values(i)     ! penalty cut weight
                
                    do j = 1,2*nRhoPerRegion-3 ! so j = 1,1 for nRhoPerRegion=2 or j=1,3 for nRhoPerRegion=3
    
                        ind1_a = (iregion1-1)*nRhoPerRegion + eps_aniso(j)    ! anisotropy component j 
                        ind2_a = (iregion2-1)*nRhoPerRegion + eps_aniso(j)    ! anisotropy component j 
                        
                        ind1_b = (iregion1-1)*nRhoPerRegion + eps_aniso(j+1)    ! eps_aniso(j+1) is cyclic so next anisotropy component 
                        ind2_b = (iregion2-1)*nRhoPerRegion + eps_aniso(j+1)    !  
                        
                        iFree1_a = iFreeParam(ind1_a)
                        iFree2_a = iFreeParam(ind2_a)
                        iFree1_b = iFreeParam(ind1_b)
                        iFree2_b = iFreeParam(ind2_b)
                            
                        if ( (iFree1_a > 0).and.(iFree2_a > 0) .and. &
                             (iFree1_b > 0).and.(iFree2_b > 0)  ) then ! all parameters are free, add a penalty term:
                    
                            ict = ict + 1 ! count penalty term
                            nratiospatial  = nratiospatial + 1
                            
                            if (iPass == 2) then  ! Set the penalty matrix terms:
                                                    
                            select case (cRoughnessPenalty)

                                case ('gradient')  
                                
                                    ! centroid-centroid distance:    
                                    dy = abs(centroids(1,iregion1) - centroids(1,iregion2))
                                    dz = abs(centroids(2,iregion1) - centroids(2,iregion2))
                  
                                    wy = yzPenaltyWeights(1)
                                    wz = yzPenaltyWeights(2)
               
                                    dr = sqrt( (dy/wy)**2+(dz/wz)**2)
                            
                                    weight = sqrt(area(iregion1))*sqrt(area(iregion2)/sumAreaNeighs)/dr
                            
                                case ('first_difference') ! no gradient or area weights, just vanilla 1st difference

                                    weight = 1d0

                                end select       
                                
                                weight = weight*cut_wt
                                
                                if (cAnisotropy(1:9) .ne.'tiz_ratio') then                     
                                    weight = anisotropyRatioRoughnessWeight
                                !else 'tiz_ratio'  so
                                ! R ( log10(r_z) - (log10(r_z) - log10(r_h))) =  R log10(r_h) so this roughens the 
                                ! horizontal component and we use standard weight here
                                endif
                      
                                pen%val(pen%nnz+1)    =  weight
                                pen%val(pen%nnz+2)    = -weight
                                pen%val(pen%nnz+3)    = -weight
                                pen%val(pen%nnz+4)    =  weight  
                                pen%colind(pen%nnz+1) = iFree1_a
                                pen%colind(pen%nnz+2) = iFree2_a                                                              
                                pen%colind(pen%nnz+3) = iFree1_b
                                pen%colind(pen%nnz+4) = iFree2_b
                                
                                pen%rowptr(ict)       = pen%nnz+1
                                                        
                            endif
                        
                            pen%nnz = pen%nnz + 4
                            
                        endif
    
                    enddo ! j = 1,nRhoPerRegion
                enddo ! i = 1,lt ! loop over adjacent regions
        
            enddo ! iregion1 = 1,nRegions
            if (lprintData) then 
                if (iPass == 2) write(*,'(a48,1x,i0)') 'Number of anisotropy roughness penalty rows:',  nratiospatial
            endif
            
        endif  ! anisotropic
 
        !--------------------------------------------
        ! Anisotropy penalty (e.g. || m_v - m_h || ):  
        !--------------------------------------------
         
        if ( (nRhoPerRegion > 1)                        .and.  &
             (anisotropyPenaltyWeight > 0)              .and.  &
             (cAnisotropy(1:9) .ne.'tiz_ratio')         .and.  &
             (cAnisotropy(1:17).ne.'isotropic_complex') .and.  & 
             (cAnisotropy(1:12).ne.'isotropic_ip')  ) then  
          
            do iregion1 = 1,nRegions
                
                do j = 1,2*nRhoPerRegion - 3  ! so j = 1,1 for nRhoPerRegion=2 or j=1,3 for nRhoPerRegion=3
            
                    ind1 = (iregion1-1)*nRhoPerRegion + eps_aniso(j)    ! eps(j) = j
                    ind2 = (iregion1-1)*nRhoPerRegion + eps_aniso(j+1)  ! eps(j+1) is cyclic

                    iFree1 = iFreeParam(ind1)
                    iFree2 = iFreeParam(ind2)    
                
                    if ( (iFree1 > 0).and.(iFree2 > 0) ) then ! both parameters are free, add a penalty term:
                
                        ict = ict + 1 ! count penalty term                   
                        naniso = naniso + 1
                    
                        if (iPass == 2) then  ! Set the penalty matrix terms:
    
                            pen%val(pen%nnz+1)    =  anisotropyPenaltyWeight
                            pen%val(pen%nnz+2)    = -anisotropyPenaltyWeight
                            pen%colind(pen%nnz+1) = iFree1
                            pen%colind(pen%nnz+2) = iFree2
                            pen%rowptr(ict)  = pen%nnz+1
                  
                        endif
              
                        pen%nnz = pen%nnz + 2     
                            
                    endif
                
                enddo   
            
           

            enddo !iregion1 = 1,nRegions     
 
            if (iPass == 2) write(*,'(a48,1x,i0)') 'Number of anisotropy penalty rows:', naniso 
                 
        endif  ! anisotropic
        
        !------------------------------------------------------------------------------------------
        ! Special case for 'tiz_ratio' parameterization.  Anisotropy penalty is applied to 2nd parameter only
        ! since log10(rz/rh) = m_z - m_h.
        !------------------------------------------------------------------------------------------

        if ( (cAnisotropy(1:9) == 'tiz_ratio') .and. ( anisotropyPenaltyWeight > 0) ) then
        
         ! 2nd parameter is log10(z/h) ratio:      
            do iregion1 = 1,nRegions

                ind2 = (iregion1-1)*nRhoPerRegion + 2  ! 2nd param is log10(z/h)

                iFree2 = iFreeParam(ind2)    

                if (iFree2 > 0)  then ! free parameter, add a penalty term:

                    ict = ict + 1 ! count penalty term                   
                    naniso = naniso + 1

                    if (iPass == 2) then  ! Set the penalty matrix terms:

                        pen%val(pen%nnz+1)    = anisotropyPenaltyWeight
                        pen%colind(pen%nnz+1) = iFree2
                        pen%rowptr(ict)       = pen%nnz+1
         
                    endif
                    
                    pen%nnz = pen%nnz + 1   
                    
                endif


            enddo !iregion1 = 1,nRegions     

            if (iPass == 2) write(*,'(a48,1x,i0)') 'Number of anisotropy penalty rows:', naniso 

        endif
                            
        
        !------------------------------------
        ! insert any new penalties below here
        !------------------------------------        
                          
        ! don't forget about last 'extra' entry in the rowptr array:
        if (iPass == 2) pen%rowptr(ict+1) = pen%nnz+1
            
    enddo ! ipass
    
    if (lprintData) then 
        write(*,*) ' '
        write(*,'(a48,1x,i0)') 'Total number of penalty matrix rows:',pen%nrows 
        write(*,'(a48,1x,i0)') 'Total number of matrix non-zeros:', pen%nnz 
        write(*,*) ' '    
    endif

    call deallocate_trimesh(mesh)
    
    deallocate(regionAdjList,centroids,area,indexes,values)
    if (allocated(cutSegments)) deallocate(cutSegments)
    
    if (lPrintDebug) write(*,*)  '...leaving generate_PenaltyMatrix' 
    
    
    end subroutine generate_PenaltyMatrix   
      
!-----------------------------------------------------------------------------------------------------------------------------------        
!-------------------------------------------------------------------------------------------------------------------intersectionTest        
!-----------------------------------------------------------------------------------------------------------------------------------  
    logical function intersectionTest(segTest,segments)
 
    real(RealPrec), dimension(2,2),     intent(in)  :: segTest  ! columns for two points. Rows are y an z position
    real(RealPrec), dimension(:,:,:),   intent(in)  :: segments            
    
    integer             :: iseg    
    logical             :: loverlap
    
    intersectionTest = .false.
    
    !
    ! Loop over all segments and test if segTest intersects:
    !
    do iseg = 1,size(segments,3)
    
        !
        ! Rectangle test: do bounding rectangles for segTest and segments(:,:,iseg) overlap?
        !
        
        loverlap = bounding_box_overlap(segTest,segments(:,:,iseg))
        
        if (loverlap) then ! segments might overlap, do more expensive intersection test
            
            intersectionTest = intersect(segTest,segments(:,:,iseg))
            
            if (intersectionTest) exit ! do loop, since one intersection is enough
        
        endif
        
    enddo
    
    end function intersectionTest
    
!-----------------------------------------------------------------------------------------------------------------------------------        
!-------------------------------------------------------------------------------------------------------------- bounding_box_overlap        
!-----------------------------------------------------------------------------------------------------------------------------------        
    logical function bounding_box_overlap(sega,segb)
!
! Returns true if bounding boxes of sega and segb overlap
!    
    
    real(RealPrec), dimension(2,2), intent(in) :: sega,segb  ! each column is y,z for a segment endpoint
    
    real(RealPrec)  :: ay1,ay2,az1,az2,by1,by2,bz1,bz2, tol
    logical         :: l(4)
    
    ay1 = minval(sega(1,:))
    ay2 = maxval(sega(1,:))
    az1 = minval(sega(2,:))
    az2 = maxval(sega(2,:))   
    by1 = minval(segb(1,:))
    by2 = maxval(segb(1,:))
    bz1 = minval(segb(2,:))
    bz2 = maxval(segb(2,:))       
 
    tol = epsilon(tol)*100.
    !write(*,*) 'tol: ',tol
 
    ! if any of these are true, rectangles do not overlap:
    l(1) = ay1 - by2 > tol   ! true if left of sega is right of right side of segb
    l(2) = by1 - ay2 > tol   ! true if left of segb is right of right side of segb
    l(3) = az1 - bz2 > tol   ! true if base of sega is above of top of segb
    l(4) = bz1 - az2 > tol   ! true if base of segb is above of top of sega
    
    bounding_box_overlap =  (.not.(any(l)))
    
    end function bounding_box_overlap
    
!-----------------------------------------------------------------------------------------------------------------------------------        
!------------------------------------------------------------------------------------------------------------------------- intersect        
!-----------------------------------------------------------------------------------------------------------------------------------        
    logical function intersect(sega,segb)
!
! Returns true if sega and segb intersect, including special case with shared endpoint
!    
    
    real(RealPrec), dimension(2,2), intent(in) :: sega,segb  ! each column is y,z for a segment endpoint
    
    real(RealPrec)  :: da(2),db(2),da_cross_db, bma(2), tol, p1, p2,bma_cross_da,bma_cross_db,pa,pb
    
    da  = sega(:,2) - sega(:,1)
    db  = segb(:,2) - segb(:,1)
    bma = segb(:,1) -  sega(:,1) 
    
    da_cross_db = da(1)*db(2) - da(2)*db(1) 
    
    bma_cross_da = bma(1)*da(2) - bma(2)*da(1) 
    bma_cross_db = bma(1)*db(2) - bma(2)*db(1) 
    
    tol = epsilon(tol)*1000.
  
    intersect = .false. 
  
    if (abs(da_cross_db) <= tol)  then  ! segments are parallel
        
        if ( abs(bma_cross_da) < tol ) then  ! colinear
        
            ! project segb endpoints onto sega
            p1 = dot_product(bma,da) /  dot_product(da,da)
            p2 = p1 + dot_product(db,da) /  dot_product(da,da)
            
            if ( (p1 >= 0.- tol) .and. (p2 <= 1. + tol) ) then
                !write(*,*) ' intersection from  ( (p1 >= 0.) .and. (p2 <= 1) ) !'
                intersect = .true.
            endif
     
       !  else ! abs(bma_cross_da) > tol and parallel lines are non-intersecting

        endif
        
    else ! segments not parallel, see if they intersect:
        
        pa = bma_cross_db / da_cross_db
        pb = bma_cross_da / da_cross_db
        
        if ( (pa >= 0.-tol) .and. (pa <= 1.+tol) .and. (pb >= 0.-tol) .and. (pb <= 1.+tol) ) then
            intersect = .true.
!              write(*,*) ' intersection! pa,pb: ',pa,pb
!              write(*,*) ' intersection! sega point ', sega(:,1) + pa*da
!              write(*,*) ' intersection! segb point ', segb(:,1) + pb*db 
        endif
    
    endif

    
    end function intersect    
    
!-----------------------------------------------------------------------------------------------------------------------------------        

    
    end module  mare2dem_penaltymatrix