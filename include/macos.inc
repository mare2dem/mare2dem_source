#
# Make include file for compiling MARE2DEM on MacOS systems with OpenMPI built
# around the *free* Intel oneAPI toolkits. 
#
#--------------------------------------------------------------------------------------------------
#
# General Installation Instructions
#
# 1) Install the oneAPI toolkit from Intel
#
#     https://www.intel.com/content/www/us/en/developer/tools/oneapi/toolkits.html 
# 
#     - Make sure you install both of these components from the Intel oneAPI:
#
#           Intel® oneAPI Base Toolkit
#           Intel® oneAPI HPC Toolkit 
# 
#     - After installing these, add this line to your ~/.profile file (for zsh
#       and bash environments):
#
#           source /opt/intel/oneapi/setvars.sh --force > /dev/null
# 
#       This tells your terminal shell where to find the Intel compilers. The
#       "--force > /dev/null" is optional and makes the terminal NOT show some
#       annoying oneAPI environment settings messages every time you open a new
#       shell. 
#
#     - Complete these additional commands to make the BLACS MKL library need with
#       OpenMPI:
#           1) cd /opt/intel/oneapi/mkl/latest/interfaces/mklmpi
#           2) sudo make libintel64 interface=lp64
#       This puts libmkl_blacs_custom_lp64.a in /opt/intel/oneapi/mkl/latest/lib
#
# 2) Install the OpenMPI compiler wrappers 
#
#      - You must do this AFTER installing the Intel compilers, as well as after
#        every time you update them.
#      - Download the free Open MPI compiler wrappers from openmpi.org/
#      - unzip, open terminal and cd to the top level openmpi directory.
#      - Issue the terminal command: 
#            sudo ./configure --prefix=/opt/openmpi FC=ifort CC=icc CXX=icc
#        and wait forever for it to configure (30 minutes or more)
#      - **BUG FIX**: Open the file "libtool" (in the top openmpi directory) and change
#        line 11837 to 11842 to be:
#           # Additional compiler flags for building library objects.
#	         pic_flag=" -fno-common -fPIC"
#	         # How to pass a linker flag through the compiler.
#	         wl="-Wl,"
#        If you don't do this, you might later on (when running MARE2DEM) see
#        the error message: "Undefined symbols for architecture x86_64:
#	                          "_ompi_buffer_detach_f08", referenced from:"
#        You have been warned.
#      - Issue the terminal command: 
#           sudo make all install
#        and then wait forever for it to build OpenMPI.
#    
# 3) Build MARE2DEM:
#
#     - In terminal, cd to mare2dem_source directory and issue command:
#           make INCLUDE=./include/make_macos.inc
#       then wait forever for MARE2DEM to compile.
#
# If you run into any problems with the compiler settings, you can run 
# " make clean_all"  to clean everything, then repeat 3) above after updating
# the compiler settings.
#
# That's it. Good luck and godspeed!
#
#--------------------------------------------------------------------------------------------------

# MPI-Fortran compiler and compiler flags:  
   FC           = /opt/openmpi/bin/mpifort   # Open MPI built around the Intel Fortran compiler
   FFLAGS       = -cxxlib -O2 -fpp    
   
# MPI C and C++ compiler and compiler flags:    
   CC           = /opt/openmpi/bin/mpicc    # Open MPI built around the Intel C compiler
   CFLAGS       = -O2    
  
# Options for Triangle.c: 
    TRICOPTS = -O2  -fp-model precise -fp-model source # optimizations for Triangle compiler on Intel icc requires the -fp args

# Intel Math Kernel Library:  
    MKLLIB = -L$(MKLROOT)/lib -I$(MKLROOT)/include -lmkl_intel_lp64  -lmkl_sequential -lmkl_core -lpthread -lm -ldl
    
#  You can use Intel's link line advisor to get the correct MKLLIB link settings for your system:
#             https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor
#
 
#			
# For ScaLAPACK, use these commands to build the libraries:                                                                      
#
    ARCH = xiar   
    ARCHFLAGS = ruv
    RANLIB = ranlib

#
# Fix for annoying MacOS Firewall "...accept incoming connections"
# pop-up messages for every MPI process every time MARE2DEM is run
#
    MACOSCODESIGN="codesign --force --deep --sign - ./MARE2DEM"
