!-----------------------------------------------------------------------
!
!    Copyright 2012-2021
!    Kerry Key
!    Lamont-Doherty Earth Observatory, Columbia University
!    kkey@ldeo.columbia.edu
!
!    This file is part of MARE2DEM.
!
!    MARE2DEM is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    MARE2DEM is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MARE2DEM.  If not, see <http://www.gnu.org/licenses/>.
!
!-----------------------------------------------------------------------

! mkl_dss_solver (double precision complex and real options)
! A Fortran module for calling the sparse LU solver included in the Intel MKL (pardiso)
! Separate subroutines are available for factoring and solving a system
! so that the factors can be reused for multiple right-hand-sides.
!


include 'mkl_dss.f90'
include 'mkl_pardiso.f90'

module intelmkl_solver

    use mkl_dss
    use mkl_pardiso

    implicit none
!
! Derived type to store intel mkl variables for a given matrix factorization
!

    ! mkl dss solver
    public :: mkl_dss_zfactor  ! Step 1. Factor a complex sparse matrix: A = LU.
    public :: mkl_dss_zsolve   ! Step 2. Solve the linear system: A x = LU x = b.
    public :: mkl_dss_zfree    ! Step 3. Deallocate the memory for the factors LU.

    public :: mkl_dss_dfactor  ! Step 1. Factor a complex sparse matrix: A = LU.
    public :: mkl_dss_dsolve   ! Step 2. Solve the linear system: A x = LU x = b.
    public :: mkl_dss_dfree    ! Step 3. Deallocate the memory for the factors LU.


    ! mkl_pardiso interface:
    public :: mkl_pardiso_zfactor  ! Step 1. Factor a complex sparse matrix: A = LU.
    public :: mkl_pardiso_zsolve   ! Step 2. Solve the linear system: A x = LU x = b.
    public :: mkl_pardiso_zfree    ! Step 3. Deallocate the memory for the factors LU.


    ! private variables for mkl_pardiso
    integer,    private     :: maxfct, mnum, mtype, phase, idum(1), iparm(64), msglvl, error
    complex(8), private     :: zdum(1)

contains

!-----------------------------------------------------------------------------------------------------------------------------------
!--------------------------------------------------------------------------------------------------------------- mkl_pardiso_zfactor
!-----------------------------------------------------------------------------------------------------------------------------------
subroutine mkl_pardiso_zfactor(pt,Az,Ai,Ap,mem)

!
! Arguments:
!
    type(mkl_pardiso_handle), intent(out)	:: pt(64)
    complex(8), dimension(:), intent(in)    :: Az
    integer, dimension(:), intent(in)       :: Ai, Ap
    real(8), intent(out)                    :: mem


!
! Local variables:
!
    integer    :: n, nz, nrhs

!
! Initialize:
!
    pt(:)%DUMMY = 0
    error  = 0
!
! Solver settings:
!
    iparm = 0
    iparm(1)  = 1
    iparm(2)  = 2 ! the nested dissection algorithm from the METIS package
    iparm(6)  = 1 ! solution overwrites vector b. Instructions note "The array x is always used."
    iparm(8)  = 0 ! # iterative refinement steps
    iparm(10) = 13 !8 13 15 ! perturb the pivot elements with 1E-13
    iparm(11) = 1 ! 1 = Enable scaling.


    msglvl = 0
    mtype  = 6 ! complex symmetric  ! 13 ! complex unsymmetric
    maxfct = 1
    mnum   = 1


    !
    ! Get matrix size
    !
    n    = ubound(Ap,1) - 1
    nz   = Ap(n+1) - 1
    nrhs = 1

    !
    ! Reorder and factor the matrix:
    !
    phase = 12
    call pardiso(pt, maxfct, mnum, mtype, phase, n, Az, Ap, Ai, idum, nrhs, iparm, msglvl, zdum, zdum, error)

    mem =  max(iparm(15), iparm(16)+iparm(17))*1024. ! Peak factorization memory in bytes


    if (error /= 0) write(*,*) 'mkl_pardiso_zsolve: The following ERROR was detected: ', error

end subroutine mkl_pardiso_zfactor

!-----------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------ mkl_pardiso_zsolve
!-----------------------------------------------------------------------------------------------------------------------------------
subroutine mkl_pardiso_zsolve(pt,Az,Ai,Ap,b_in)

    type(mkl_pardiso_handle), intent(inout)   	:: pt(64)
    complex(8), dimension(:),   intent(in)     	:: Az
    integer,    dimension(:),   intent(in)      :: Ai, Ap
    complex(8), dimension(:,:), intent(inout)   :: b_in


    integer    :: i, n, nrhs, offset
    complex(8), dimension(:), allocatable :: b,x

    n    = size(b_in,1)
    nrhs = size(b_in,2)

    allocate(b(n*nrhs),x(n*nrhs))
    offset = 0
    do i = 1,nrhs
        offset = (i-1)*n
        b(1+offset:n+offset) = b_in(:,i)
    enddo

    phase = 33
    call pardiso(pt, maxfct, mnum, mtype, phase, n, Az, Ap, Ai, idum, nrhs, iparm, msglvl, b, x, error)

    if (error /= 0) write(*,*) 'mkl_pardiso_zsolve: The following ERROR was detected: ', error
    offset = 0
    do i = 1,nrhs
        offset = (i-1)*n
        b_in(:,i) = b(1+offset:n+offset)
    enddo

    deallocate(b,x)

end subroutine mkl_pardiso_zsolve

!-----------------------------------------------------------------------------------------------------------------------------------
!----------------------------------------------------------------------------------------------------------------- mkl_pardiso_zfree
!-----------------------------------------------------------------------------------------------------------------------------------
subroutine mkl_pardiso_zfree(pt)
!
! Free numeric factorization memory:
!
    type(mkl_pardiso_handle), intent(inout) :: pt(64)

    integer    :: n,nrhs

    n     = 1  ! kwk debug: do n and nrhs need to be correct here?
    phase = -1 ! release internal memory
    call pardiso(pt, maxfct, mnum, mtype, phase, n, zdum, idum, idum, idum, nrhs, iparm, msglvl, zdum, zdum, error)

    if (error /= 0) write(*,*) 'mkl_pardiso_zfree: The following ERROR was detected: ', error

end subroutine mkl_pardiso_zfree
!-------------------------------------------------------------------------
!----------------------------------------------------------mkl_dss_zfactor
!-------------------------------------------------------------------------
subroutine mkl_dss_zfactor(this,Az,Ai,Ap)
!
! Factors a complex matrix A, with pointers to solution being stored in
! intelmkl derived type variable "this".
!

!
! Arguments:
!
    TYPE(MKL_DSS_HANDLE), intent(inout)   :: this

    complex(8), dimension(:), intent(in)  :: Az
    integer, dimension(:), intent(inout)  :: Ai, Ap     ! modified for c indexing here, corrected before returing

    integer ::  opt


!
! Local variables:
!
    integer    :: n, nz, info, perm(1)

!
! Get matrix size
!
    n = ubound(Ap,1) - 1
    nz = Ap(n+1) - 1

!
! Initialize the solver:
!
    opt  = MKL_DSS_DEFAULTS
    info = dss_create( this, opt )

    if (info /= MKL_DSS_SUCCESS) then
        write(*,*) 'Error calling DSS_CREATE, info = ',info
        stop
    endif

!
! Define the non-zero structure of the matrix:
!
    opt = MKL_DSS_SYMMETRIC_COMPLEX
    info = dss_define_structure( this, opt , Ap, n, n, Ai, nz )

    if (info /= MKL_DSS_SUCCESS) then
        write(*,*) 'Error calling DSS_DEFINE_STRUCTURE, info = ',info
        stop
    endif

!
! Reorder the matrix:
!
    perm(1) = 0
    opt = MKL_DSS_METIS_OPENMP_ORDER !MKL_DSS_AUTO_ORDER
    info = dss_reorder( this, opt, perm )
    if (info /= MKL_DSS_SUCCESS) then
        write(*,*) 'Error calling DSS_REORDER, info = ',info
        stop
    endif


!
! Factor the matrix:
!
    opt = MKL_DSS_INDEFINITE
    info = dss_factor_complex( this, opt, Az )
    if (info /= MKL_DSS_SUCCESS) then
        write(*,*) 'Error calling DSS_FACTOR_REAL, info = ',info
        stop
    endif

end subroutine mkl_dss_zfactor

!-------------------------------------------------------------------------
!-----------------------------------------------------------mkl_dss_zsolve
!-------------------------------------------------------------------------
subroutine mkl_dss_zsolve(this,b)

    implicit none

!
! Arguments:
!
    TYPE(MKL_DSS_HANDLE), intent(inout)      :: this
    complex(8), dimension(:), intent(inout)  :: b

!
! Local variables:
!
    integer :: n, nrhs, info, opt


    complex(8), dimension(:), allocatable :: sol

!
! Allocate temporary solution array
!
    n = ubound(b,1)
    nrhs =  1 ! !ubound(b,2)

    allocate(sol(n))

!
! Solve the linear system Ax=b:
!
    opt = MKL_DSS_DEFAULTS + MKL_DSS_REFINEMENT_OFF
    info = dss_solve_complex(this, opt, b, nrhs, sol )

    if (info /= MKL_DSS_SUCCESS) then
        write(*,*) 'Error calling DSS_SOLVE_COMPLEX, info = ',info
        stop
    endif

    b = sol

    deallocate(sol)

end subroutine mkl_dss_zsolve

!-------------------------------------------------------------------------
!------------------------------------------------------------mkl_dss_zfree
!-------------------------------------------------------------------------
subroutine mkl_dss_zfree(this)

    implicit none

    TYPE(MKL_DSS_HANDLE), intent(inout)    :: this

    integer :: info, opt

!
! Free numeric factorization memory:
!
    opt = MKL_DSS_DEFAULTS
    info = dss_delete( this, opt )
    if (info /= MKL_DSS_SUCCESS) then
        write(*,*) 'Error calling DSS_DELETE, info = ',info
        stop
    endif

end subroutine mkl_dss_zfree

!-------------------------------------------------------------------------
!----------------------------------------------------------mkl_dss_dfactor
!-------------------------------------------------------------------------
subroutine mkl_dss_dfactor(this,Az,Ai,Ap)
!
! Factors a complex matrix A, with pointers to solution being stored in
! intelmkl derived type variable "this".
!

!
! Arguments:
!
    TYPE(MKL_DSS_HANDLE), intent(inout)   :: this

    real(8), dimension(:), intent(in)     :: Az
    integer, dimension(:), intent(inout)  :: Ai, Ap     ! modified for c indexing here, corrected before returing

    integer ::   opt


!
! Local variables:
!
    integer    :: n, nz, info, perm(1)

!
! Get matrix size
!
    n = ubound(Ap,1) - 1
    nz = Ap(n+1) - 1

!
! Initialize the solver:
!
    opt  = MKL_DSS_DEFAULTS
    info = dss_create( this, opt )

    if (info /= MKL_DSS_SUCCESS) then
        write(*,*) 'Error calling DSS_CREATE, info = ',info
        stop
    endif

!
! Define the non-zero structure of the matrix:
!

    info = dss_define_structure( this, MKL_DSS_SYMMETRIC, Ap, n, n, Ai, nz )
    if (info /= MKL_DSS_SUCCESS) then
        write(*,*) 'Error calling DSS_DEFINE_STRUCTURE, info = ',info
        stop
    endif

!
! Reorder the matrix:
!
    perm(1) = 0
    opt = MKL_DSS_AUTO_ORDER
    info = dss_reorder( this, opt, perm )
    if (info /= MKL_DSS_SUCCESS) then
        write(*,*) 'Error calling DSS_REORDER, info = ',info
        stop
    endif


!
! Factor the matrix:
!
    opt = MKL_DSS_POSITIVE_DEFINITE   !MKL_DSS_INDEFINITE
    info = dss_factor_real( this, opt, Az )
    if (info /= MKL_DSS_SUCCESS) then
        write(*,*) 'Error calling DSS_FACTOR_REAL, info = ',info
        stop
    endif

end subroutine mkl_dss_dfactor

!-------------------------------------------------------------------------
!-----------------------------------------------------------mkl_dss_dsolve
!-------------------------------------------------------------------------
subroutine mkl_dss_dsolve(this,b)

    implicit none

!
! Arguments:
!
    TYPE(MKL_DSS_HANDLE), intent(inout)      :: this
    real(8), dimension(:), intent(inout)  :: b

!
! Local variables:
!
    integer :: n, nrhs, info, opt


    real(8), dimension(:), allocatable :: sol

!
! Allocate temporary solution array
!
    n = ubound(b,1)
    nrhs =  1 ! !ubound(b,2)

    allocate(sol(n))

!
! Solve the linear system Ax=b:
!
    opt = MKL_DSS_DEFAULTS + MKL_DSS_REFINEMENT_OFF
    info = dss_solve_real(this, opt, b, nrhs, sol )

    if (info /= MKL_DSS_SUCCESS) then
        write(*,*) 'Error calling DSS_SOLVE_COMPLEX, info = ',info
        stop
    endif

    b = sol

    deallocate(sol)

end subroutine mkl_dss_dsolve

!-------------------------------------------------------------------------
!------------------------------------------------------------mkl_dss_dfree
!-------------------------------------------------------------------------
subroutine mkl_dss_dfree(this)

    implicit none

    TYPE(MKL_DSS_HANDLE), intent(inout)    :: this

    integer :: info

!
! Free numeric factorization memory:
!
    info = dss_delete( this, MKL_DSS_DEFAULTS )
    if (info /= MKL_DSS_SUCCESS) then
        write(*,*) 'Error calling DSS_DELETE, info = ',info
        stop
    endif

end subroutine mkl_dss_dfree


end module intelmkl_solver
